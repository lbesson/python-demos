#! /usr/bin/env python
# -*- coding: utf-8 -*-
"""
About prime numbers. From what one MEC student (14XJ00142) asked me to work on.

@date: Monday 02nd of March, 08:06:42, 2015.
@author: Lilian Besson for CS101 course at Mahindra Ecole Centrale 2015.
@licence: GNU Public License version 3.
"""

from math import sqrt

# We use momoization to keep in memory a *set* of all the prime numbers that we found
set_of_primes = {2, 3, 5, 7, 11, 13, 17, 19, 23}

def isprime(n):
    """ Test if n is prime, by looking for a non-trivial divisor in the range [2, sqrt(n)].
    As soon as one such divisor is found, False is return. Else, True is returned at the end."""
    if n in set_of_primes:
        return True  # so asking twice the same isprime(n) will be very efficient
    for i in xrange(2, 1+int(sqrt(n))):
        if n%i == 0:
            return False
    set_of_primes.add(n)  # if we return True, we keep this in memory
    return True


def largest_prime_factor(n):
    """ Finds the biggest prime factor of n, without really constructing the list of its divisors."""
    largest_yet = None
    if n%2 == 0:
        largest_yet = 2

    start = int(sqrt(n))
    if start%2 == 0:
        start += 1  # we need to start from an odd number, upto 3
    for d in xrange(start, 2, -2):
        if n%d == 0 and d > largest_yet and isprime(d):
            largest_yet = d
            break  # as we go backward, from the biggest d to 3, useless to continue!

    if largest_yet is None:
        largest_yet = n  # the number n itself if a prime number !
        set_of_primes.add(n)

    return largest_yet


if __name__ == '__main__':
    # Testing all this if the script is called as main program

    from random import randint

    for test in xrange(100):
        n = int(randint(1e10, 9e12))  # adapt here the size upto which we will select random integers
        print "For the number n =", n
        print "   its largest prime factor seems to be", largest_prime_factor(n)

    # This one might take a while
    n = 1234567764613455416
    print "For the last and big number n =", n
    print "   its largest prime factor seems to be", largest_prime_factor(n)

    print "With all these examples, my current set of prime numbers is", set_of_primes
