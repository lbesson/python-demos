#! /usr/bin/env python
# -*- coding: utf-8 -*-
"""
Simple measurements on images, using scipy.ndimage.

Based on the paragraph 5.10.4 "Measurements on images" of Python Scientific lecture notes, Release 2013.2 beta (euroscipy 2013).

@date: Mon Mar 30 21:19:58 2015.
@author: Lilian Besson for CS101 course at Mahindra Ecole Centrale 2015.
@licence: MIT Licence (http://lbesson.mit-license.org).
"""

import numpy as np
from scipy import ndimage
#import pylab as pl
import matplotlib.pyplot as plt

fig = plt.figure(1)

plt.subplot(131)
plt.axis('off')

# Generate a nice synthetic binary image.
x, y = np.indices((200, 200))
sig = np.sin(2*np.pi*x/50.)*np.sin(2*np.pi*y/50.)*(1+x*y/50.**2)**2

plt.title("Synthetic image")
plt.imshow(sig)
#plt.colorbar(orientation='horizontal')

# Mask
mask = sig > 1

plt.subplot(132)
plt.axis('off')

plt.title("Mask (binary)")
plt.imshow(mask, cmap='gray')

# Now we look for various information about the objects in the image:
labels, nb = ndimage.label(mask)  # pylint: disable=W0633
print "The function ndimage.label gave", nb, "different labels."

areas = ndimage.sum(mask, labels, xrange(1, labels.max()+1))
print "We used ndimage.sum with the mask and labels to compute this array of areas :", areas

maxima = ndimage.maximum(sig, labels, xrange(1, labels.max()+1))
print "ndimage.maximum with these labes gave", maxima

print "Let us find the part of the image close to the label #3:"
sl = ndimage.find_objects(labels == 1)
print sl

plt.subplot(133)
plt.axis('off')

plt.title("Labels ({} different areas)".format(nb+1))
#plt.imshow(sig[sl[0]])  # to show the first label
plt.imshow(labels)

plt.savefig("Measurements_on_images_with_scipy_ndimage.png")
