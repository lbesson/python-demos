#! /usr/bin/env python
# -*- coding: utf-8 -*-
"""
Freezer script for my Two Volumes demo.
Still HIGHLY experimental.

More information on:

- https://docs.python.org/2/distutils/setupscript.html
- http://cx-freeze.readthedocs.org/en/latest/distutils.html#distutils

@date: Mon Feb 25 16:15:21 2015.
@author: Lilian Besson for CS101 course at Mahindra Ecole Centrale 2015.
@license: GNU Public License version 3.
"""

import sys
from cx_Freeze import setup, Executable

# Dependencies are automatically detected, but it might need fine tuning.
# FIXME: only TCL/TK is copied ??
build_exe_options = dict(
#    packages = ['matplotlib', 'numpy'],
    includes = [],
#    excludes = ['pydoc'],
    path = sys.path,
    include_msvcr = True,
    compressed = True,
    optimize = 0,  # 0, 1 or 2
    init_script = 'Console'  # FIXME: it load the thing from C:\Anaconda\... and not from the local folder
)

# Options for the executables (http://cx-freeze.readthedocs.org/en/latest/distutils.html#cx-freeze-executable)
compress = True
icon = None  # 'bomberman.ico'


# For a graphical program (not the case here)
# FIXME: after, the client and IA might be like this
base_gui = None
if sys.platform == "win32":
    base_gui = "Win32GUI"

# For a console program
# FIXME I want a Windows console to open automatically when I execute the .exe file
base_console = 'Console'


print "Building the application"
executables = [
    Executable('DemoTwoVolumes__cx_Freeze.py',
               base=base_console, icon=icon, compress=compress)
]

setup(name='Python PyLab demo',
      version = '1.0',
      description = 'Python PyLab demo : two volumes',
      license = 'GNU Public License version 3',
      author='Lilian Besson for CS101 course at Mahindra Ecole Centrale 2015.',
      author_email='CS101@crans.org',
      url='http://perso.crans.org/besson/cs101/',
      options = dict(build_exe = build_exe_options),
      executables = executables,
)

print "Done with building. Check the dist folder for more details."
