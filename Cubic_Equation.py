#! /usr/bin/env python
# -*- coding: utf-8 -*-
"""
Solving linear, quadratic and cubic equations with pure Python.

Reference: https://en.wikipedia.org/wiki/Cubic_function#Cardano.27s_method

@date: Wed Mar 04 23:13:36 2015.
@author: Lilian Besson for CS101 course at Mahindra Ecole Centrale 2015.
@licence: MIT Licence (http://lbesson.mit-license.org).
"""

import sys, math, cmath
from numpy import roots, polyval
from numpy.polynomial import Polynomial as P


# %% Conversion functions

def approx_complex(z, precision=8):
    """ Approximate a number (float or complex) with a certain precision.
    Default one is 8 digits (enough to check our functions). """
    try:
        z = complex(z)
    except:
        print "Exception:", sys.exc_info()[0]
    if hasattr(z, 'real') and hasattr(z, 'imag'):
        return round(float(z.real), precision) + 1j * round(float(z.imag), precision)
    else:
        return round(float(z))


def set_of_solutions(inputarray, precision=8):
    """ Used to transform an array/list of solutions to a set of approximated solutions (rounded)."""
    try:
        if hasattr(inputarray, '__iter__'):
            return { approx_complex(z, precision) for z in inputarray }
        else:
            return set( [ approx_complex(inputarray, precision) ] )
    except:
        print "Exception:", sys.exc_info()[0]
        # inputarray might not be an array/list in fact...
        try:
            print "set_of_solutions: error 1 with input", inputarray
            return { approx_complex(inputarray) }
        except:
            print "Exception:", sys.exc_info()[0]
            try:
                print "set_of_solutions: error 2 with input", inputarray
                return set(inputarray)
            except:
                print "Exception:", sys.exc_info()[0]
                return { inputarray }


def check_solutions(p, solutions, tolerance=1e-8):
    """ Check that each elements of the list/array/set solutions is a good (numerical) root of the polynomial p.
    Uses numpy.polyval to evaluate the polynomial function p (given as a list/array of coefficients, p[0]*x**(N-1) + ... + p[N-1])."""
    return all( [ round(abs(polyval(p, x))) < tolerance for x in solutions ] )


# %% Linear equation

# Solution from numpy.polynomial.Polynomial
linear_equation_P = lambda (a, b): P([b, a]).roots()

# Solution from numpy.roots
linear_equation_roots = lambda (a, b): roots([a, b])


def linear_equation(a, b):
    """ Solution for the linear equation ax + b = 0.
    https://en.wikipedia.org/wiki/Linear_equation.
    """
    if a == 0:
        raise ValueError("No solution for this linear equation a={}, b={}.".format(a, b))
    else:
        return -b / float(a)


assert set_of_solutions(linear_equation_P((2, 5))) == {-2.5}
assert set_of_solutions(linear_equation_roots((2, 5))) == {-2.5}
assert set_of_solutions(linear_equation(2, 5)) == {-2.5}
print "For a = 2, b = 5, the solution for ax+b = 0 is {}.".format(linear_equation(2, 5))


# %% Quadratic equation

# Solution from numpy.polynomial.Polynomial
quadratic_equation_P = lambda (a, b, c): P([c, b, a]).roots()

# Solution from numpy.roots
quadratic_equation_roots = lambda (a, b, c): roots([a, b, c])


def quadratic_equation(a, b, c):
    """ Solution for the quadratic equation ax**2 + bx + c = 0.
    https://en.wikipedia.org/wiki/Quadratic_equation.
    """
    if a == 0:
        return linear_equation(b, c)
    else:
        delta = b**2 - 4*a*c
        if delta > 0:
            x1 = (-b - math.sqrt(delta)) / (2*a)
            x2 = (-b + math.sqrt(delta)) / (2*a)
            x1, x2 = sorted((x1, x2))  # Experimental
        elif delta == 0:
            x1 = (-b) / (2*a)
            x2 = (-b) / (2*a)
        elif delta < 0:
            # We need to use cmath.sqrt because delta < 0 !]
            x1 = (-b**2 - cmath.sqrt(delta)) / (2*a)
            x2 = (-b**2 + cmath.sqrt(delta)) / (2*a)
        else:
            raise ValueError("Error with the discriminant for the quadratic equation {}x**2 + {}x + {} = 0...".format(a, b, c))
        return [x1, x2]


assert set_of_solutions(quadratic_equation_P((1, 0, 9))) == {-3j, 3j}
assert set_of_solutions(quadratic_equation_roots((1, 0, 9))) == {3j, -3j}
assert set_of_solutions(quadratic_equation(1, 0, 9)) == {-3j, 3j}
print "For a = 1, b = 0, c = 9, the solutions for ax^2+bx+c = 0 are {}.".format(quadratic_equation(1, 0, 9))


# %% Cubic equation

# Solution from numpy.polynomial.Polynomial
cubic_equation_P = lambda (a, b, c, d): P([d, c, b, a]).roots()

# Solution from numpy.roots
cubic_equation_roots = lambda (a, b, c, d): roots([a, b, c, d])


def sign(x):
    """ Return the sign of x, as defined usually in mathematics:
    0 is x is 0, 1 is x > 0, -1 if x < 0, phase(x) if x is complex, 1 otherwise. """
    if x == 0:
        return 0
    elif isinstance(x, complex):
        if x.imag != 0:
            return cmath.phase(x)
        else:
            return sign(x.real)  # recursive whouhou !
#        return 1  # no sign for a complex number!
    try:
        if x > 0:
            return 1
        elif x < 0:
            return -1
    except:
        raise ValueError("Invalid argument for function sign: {}".format(x))
    return 1  # should never happen


def roots_of_z(z, n):
    """ Returns all the *approximated* n nth roots of the complex number z (as an ordered list [w_n^0, w_n^1, .., w_n^{n-1}] of size n).

    Will fail if n is zero, raising a ValueError exception.
    """
    if n == 0:
        raise ValueError("roots(z, n) cannot be called with n = 0 (with z = {}).".format(z))
    else:
        pi = math.pi
#        nth_root_of_r = abs(z)**(1.0/float(n))
        nth_root_of_r = pow(abs(z), 1.0/float(n))
        t = cmath.phase(z)
        return [ approx_complex(nth_root_of_r*cmath.exp((t+2*k*pi)*1j/n)) for k in xrange(n) ]


# The two special numbers, non-trivial 3rd roots of unity
# (offen written as j and j**2 in books, at least French ones)
zj1 = -0.5 + math.sqrt(3.0)*0.5j
#zj1 = cmath.exp(2j * math.pi / 3.0)
zj2 = zj1.conjugate()


def cubic_equation(a, b, c, d, verb=False):
    """ Solution for the cubic equation ax**3 + bx**2 + cx + d = 0.

    References:
     - https://fr.wikipedia.org/wiki/M%C3%A9thode_de_Cardan#Formules_de_Cardan
     - https://en.wikipedia.org/wiki/Cubic_function#Cardano.27s_method.
    """
    if a == 0:
        return quadratic_equation(b, c, d)
    else:
        # https://en.wikipedia.org/wiki/Tschirnhaus_transformation
        # https://en.wikipedia.org/wiki/Cubic_function#Reduction_to_a_depressed_cubic
        p = (3*a*c - (b**2)) / (3.0*(a**2))
        q = (2*(b**3) - 9*a*b*c + 27*(a**2)*d) / (27.0*(a**3))

        if p==0 and q==0:
            return [0, 0, 0]  # 0 is a triple solution!

        # Now we consider the equation t**3 + p*t + q = 0
        if verb: print "p =", p
        if verb: print "q =", q

        delta = ((q**2) / 4.0) + ((p**3) / 27.0)
        if verb: print "So delta =", delta

        if delta > 0:
            if verb: print "delta > 0: u_cube and v_cube are two distincts real numbers."
            u_cube = ((-q) / 2.0) + math.sqrt(delta)
            v_cube = ((-q) / 2.0) - math.sqrt(delta)
        elif delta == 0:
            if verb: print "delta = 0: u_cube and v_cube are one common real number."
            u_cube = v_cube = ((-q) / 2.0)
            return sorted([(3.*q/p), (-3.*q/(2*p)), ((-3.*q/(2*p)))])
        elif delta < 0:
            if verb: print "delta < 0: u_cube and v_cube are two distincts complex numbers (non-real)."
            # We need to use cmath.sqrt because delta < 0 !
            u_cube = ((-q) / 2.0) + cmath.sqrt(delta)
            v_cube = ((-q) / 2.0) - cmath.sqrt(delta)
            assert v_cube == u_cube.conjugate()
        else:  # should never happen
            raise ValueError("Error with the discriminant for the cubic equation {}x**3 + {}x**2 + {}x + {} = 0...".format(a, b, c, d))

        print "u_cube is", u_cube, "and v_cube is", v_cube

        if v_cube == 0:  # FIXME
            result = roots_of_z(u_cube, 3)
            print "v_cube is 0, so result =", result
        elif u_cube == 0:  # FIXME
            result = roots_of_z(v_cube, 3)
            print "u_cube is 0, so result =", result
        else:
            # Here we are cautious about the signs
            # and the fact that u_cube and v_cube might be complex!
            # We start by computing u = |u**3|**(1/3)
            u = pow(abs(u_cube), 1./3.)  # u is real
            if verb: print "u =", u
            if isinstance(u_cube, complex) and u_cube.imag != 0:
                if verb: print "u_cube is complex, of phase =", cmath.phase(u_cube)
                if verb: print "u is multiplied by", cmath.exp(1j*cmath.phase(u_cube) / 3.0)
                assert abs(abs(cmath.exp(1j*cmath.phase(u_cube) / 3.0)) - 1) < 1e-9
                u *= cmath.exp(1j*cmath.phase(u_cube) / 3.0)
            else:
                if verb: print "u_cube is real, of sign =", sign(u_cube)
                assert sign(u_cube) in {-1, 0, 1}
                u *= sign(u_cube)

            u = approx_complex(u)
            print "Finally, u =", u

            if verb: print "Solution(s) u can be:", roots_of_z(u_cube, 3)
            assert approx_complex(u) in roots_of_z(u_cube, 3)
            assert abs(approx_complex(u)**3 - approx_complex(u_cube)) < 1e-6

            # Now for v = |v**3|**(1/3)
            v = pow(abs(v_cube), 1./3.)  # v is real
            if verb: print "v =", v
            if isinstance(v_cube, complex) and v_cube.imag != 0:
                if verb: print "v_cube is complex, of phase =", cmath.phase(v_cube)
                if verb: print "v is multiplied by",  cmath.exp(1j*cmath.phase(v_cube) / 3.0)
                assert abs(abs(cmath.exp(1j*cmath.phase(v_cube) / 3.0)) - 1) < 1e-9
                v *= cmath.exp(1j*cmath.phase(v_cube) / 3.0)
            else:
                if verb: print "v_cube is real, of sign =", sign(v_cube)
                assert sign(v_cube) in {-1, 0, 1}
                v *= sign(v_cube)

            v = approx_complex(v)
            print "Finally, v =", v

            if verb: print "Solution(s) v can be:", roots_of_z(v_cube, 3)
            assert approx_complex(v) in roots_of_z(v_cube, 3)
            assert abs(approx_complex(v)**3 - approx_complex(v_cube)) < 1e-6

            # We compute ourself the 3 solutions
            t1 = u + v
            t2 = (u * zj1) + (v * zj2)
            t3 = (u * (zj1**2)) + (v * (zj2**2))
            result = [t1, t2, t3]
        try:
            result.sort()
        except Exception:
            pass  # unable to sort because of complex numbers
        return result


sol1 = set_of_solutions({1, zj1, zj2})
print "For a = 1, b = 0, c = 0, d = 1, the solutions for ax^3+bx^2+cx+d = 0 are {}.".format(sol1)

assert set_of_solutions(cubic_equation_P((1, 0, 0, -1))) == sol1  # OK
assert set_of_solutions(cubic_equation_roots((1, 0, 0, -1))) == sol1  # OK
assert set_of_solutions(cubic_equation(1, 0, 0, -1)) == sol1  # OK

# One more example, from https://fr.wikipedia.org/wiki/M%C3%A9thode_de_Tschirnhaus#Application_.C3.A0_la_r.C3.A9solution_des_.C3.A9quations_cubiques
# http://www.wolframalpha.com/input/?i=solve%28x^3+%2B+x+-+2+%3D+0%29
x1 = (-1 + 1j*math.sqrt(7))/2.0
x2 = x1.conjugate()

sol2 = set_of_solutions({1, x1, x2})
print "For a = 1, b = 0, c = 1, d = -2, the solutions for ax^3+bx^2+cx+d = 0 are {}.".format(sol2)

assert set_of_solutions(cubic_equation_P((1, 0, 1, -2))) == sol2  # OK
assert set_of_solutions(cubic_equation_roots((1, 0, 1, -2))) == sol2  # OK
assert set_of_solutions(cubic_equation(1, 0, 1, -2)) == sol2  # FIXME fails ?


# %% A test for the cubic equation with some randomly generated numbers
from random import randint
A, B = -10, 10
nb_test = 5
successes = 0

from sympy import solve, Poly
from sympy.abc import x


for k in xrange(1, 1+nb_test):
    a, b, c, d = randint(A, B), randint(A, B), randint(A, B), randint(A, B)
    print "\n\n# Test number {k}.\nFor a = {a}, b = {b}, c = {c}, d = {d}, we solve the equation ax^3+bx^2+cx+d = 0 :\n".format(k=k, a=a, b=b, c=c, d=d)
    sol1 = set_of_solutions(cubic_equation_P((a, b, c, d)))
#    print " - (1) With the method from numpy.polynomial.Polynomial, the solutions are {}.".format(sol1)

    sol2 = set_of_solutions(cubic_equation_roots((a, b, c, d)))
#    print " - (2) With the method from numpy.roots, the solutions are {}.".format(sol2)

    p = [a, b, c, d]
    mypoly = Poly(sum([ p[i] * x**(len(p)-i-1) for i in xrange(len(p)) ]))
    sol3 = set_of_solutions(solve(mypoly, x))
    print " - (3) With the method from sympy.solve, the solutions are {}.".format(sol3)

    # The 3 clean methods always produces the same (approximated) result
    assert sol1 == sol2 == sol3
    assert check_solutions(p, sol1)
    assert check_solutions(p, sol2)
    assert check_solutions(p, sol3)

    sol4 = set_of_solutions(cubic_equation(a, b, c, d))
    print " - (4) By my function, the solutions are {}.".format(sol4)

    if sol1 == sol2 == sol3 == sol4:
        successes += 1  # as far as now, never..
        print "\nThe {}th test is a success!".format(k)
    else:
        print "\n/!\\ The {}th test is a failure :(".format(k)

#assert successes == nb_test
print "End of my script. Rate of success: {:.2%}".format(successes/float(nb_test))
