#! /usr/bin/env python
# -*- coding: utf-8 -*-
"""
Two demos of the Numba module and its awesome JIT compiler.

It shows that adding one line (`@numba.jit`) before each function can possibly bring a speed-up of 100× or more!


More information online at http://numba.pydata.org.

@date: Sat Mar 18 12:21:29 2015.
@author: Lilian Besson for CS101 course at Mahindra Ecole Centrale 2015.
@licence: MIT Licence (http://lbesson.mit-license.org).
"""

# %% First example: without Numba
import numpy as np

def sum2d_1(arr):
    M, N = arr.shape
    result = 0.0
    for i in range(M):
        for j in range(N):
            result += arr[i,j]
    return result


N = 100
a = np.random.uniform(0, 1e8, N**2).reshape(N, N)
print(sum2d_1(a))


#For N = 10**0 :
#100000 loops, best of 3: 6.33 µs per loop
#For N = 10**1 :
#10000 loops, best of 3: 88.8 µs per loop
#For N = 10**2 :
#100 loops, best of 3: 8.02 ms per loop
#For N = 10**3 :
#1 loops, best of 3: 808 ms per loop
#For N = 10**4 :
#Too long

for k in range(0, 4):
    print "For N = 10**{} :".format(k)
    N = 10**k
    %timeit sum2d_1(np.random.uniform(0, 1e8, N**2).reshape(N, N))


# %% Now, second example with Numba JIT compiler enabled:
import numba

# jit decorator tells Numba to compile this function.
# The argument types will be inferred by Numba when function is called.
@numba.jit
def sum2d_2(arr):
    M, N = arr.shape
    result = 0.0
    for i in range(M):
        for j in range(N):
            result += arr[i,j]
    return result

N = 3
a = np.arange(N**2).reshape(N, N)
print(sum2d_2(a))


#For N = 10**0 :
#100000 loops, best of 3: 3.74 µs per loop
#For N = 10**1 :
#100000 loops, best of 3: 6.46 µs per loop
#For N = 10**2 :
#1000 loops, best of 3: 263 µs per loop
#For N = 10**3 :
#10 loops, best of 3: 26.7 ms per loop
#For N = 10**4 :
#1 loops, best of 3: 3.25 s per loop

for k in range(0, 4):
    print "For N = 10**{} :".format(k)
    N = 10**k
    %timeit sum2d_2(np.random.uniform(0, 1e8, N**2).reshape(N, N))


# %% Third example: compute and plot the Mandelbrot set using matplotlib.

import numpy as np
import numba
import matplotlib.pyplot as plt

@numba.jit
def mandel(x, y, max_iters):
    """ Given the real and imaginary parts of a complex number,
    determine if it is a candidate for membership in the Mandelbrot
    set given a fixed number of iterations.
    """
    c = complex(x,y)
    z = 0j
    for i in range(max_iters):
        z = z*z + c
        if z.real * z.real + z.imag * z.imag >= 4:
            return 255 * i // max_iters

    return 255


@numba.jit(nopython=True)
def create_fractal(min_x, max_x, min_y, max_y, image, iters):
    height = image.shape[0]
    width = image.shape[1]

    pixel_size_x = (max_x - min_x) / width
    pixel_size_y = (max_y - min_y) / height
    for x in range(width):
        real = min_x + x * pixel_size_x
        for y in range(height):
            imag = min_y + y * pixel_size_y
            color = mandel(real, imag, iters)
            image[y, x] = color

    return image


# Without numba.jit
#image = np.zeros((200, 100), dtype=np.uint8)
#10 loops, best of 3: 151 ms per loop
#image = np.zeros((700, 1400), dtype=np.uint8)
#1 loops, best of 3: 6.52 s per loop
#image = np.zeros((1500, 2667), dtype=np.uint8)
#1 loops, best of 3: 32.8 s per loop


# With numba.jit
#image = np.zeros((200, 100), dtype=np.uint8)
#100 loops, best of 3: 1.84 ms per loop
image = np.zeros((768, 1366), dtype=np.uint8)
#1 loops, best of 3: 89.2 ms per loop
#image = np.zeros((1500, 2667), dtype=np.uint8)
#1 loops, best of 3: 363 ms per loop

#%timeit create_fractal(-2.0, 1.0, -1.0, 1.0, image, 20)
create_fractal(-2.0, 1.0, -1.0, 1.0, image, 18)

plt.xticks([])
plt.yticks([])
plt.imshow(image)
#plt.gray()

#plt.savefig("Mandelbrot_fractal__with_numpy_matplotlib_and_numba.pdf", dpi=180)
#plt.savefig("Mandelbrot_fractal__with_numpy_matplotlib_and_numba.svg")
plt.savefig("Mandelbrot_fractal__with_numpy_matplotlib_and_numba.png", dpi=180)

plt.show()
# END
