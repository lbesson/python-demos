#! /usr/bin/env python
# -*- coding: utf-8 -*-
"""
Demo of PyLab for doing easy plotting for CS101 grades.
Way easier and nicer than Excel...

You can work on it by yourself to pick up bases of plotting with Python and PyLab.


@date: Wed Feb 25 17:38:00 2015.
@author: Lilian Besson for CS101 course at Mahindra Ecole Centrale 2015.
@licence: GNU Public License version 3.
"""

# We import the PyLab module, which turns out Python as a MatLab clone
from pylab import *

# List of grades, included row from the Excel sheet
grades_from_excel = [ 19, 31, 23, 79, 37, 21, 25, 24, 44, 25, 46, 30, 54, 89, 60, 24, 39, 25, 45, 27, 24, 48, 47, 17, 87, 88, 91, 77, 93, 22, 18, 24, 46, 55, 39, 33, 43, 54, 69, 50, 35, 21, 56, 46, 6, 16, 16, 40, 51, 38, 50, 22, 16, 11, 37, 23, 18, 42, 65, 62, 36, 48, 25, 14, 73, 58, 31, 41, 24, 23, 23, 49, 45, 67, 48, 72, 89, 69, 52, 82, 14, 18, 62, 34, 32, 44, 38, 27, 41, 7, 44, 42, 40, 42, 78.5, 58.5, 74, 28, 21.5, 41, 29, 17.5, 56, 23, 19.5, 25.5, 17.5, 25, 76, 72.5, 22.5, 37.5, 6, 27, 45, 84, 31, 45, 31.5, 27, 37.5, 4, 10, 67, 43.5, 67, 69.5, 41, 25, 56, 54, 24.5, 39.5, 32.5, 29, 31, 58.5, 55, 24, 65, 39, 38, 17, 31, 66, 23, 65, 56, 25, 23, 47, 21, 30, 27, 30, 21, 63, 31, 85, 26, 92, 28, 45, 31, 74, 49, 39, 37, 62, 20, 37, 86, 20, 31, 68, 82, 57, 46, 50, 23, 36, 53, 48, 24, 22, 44, 62, 62, 12, 48, 45, 88, 31, 36, 13, 16, 20, 3, 60, 52, 22, 34, 32, 4, 45, 44, 77, 22, 16, 39, 65, 65, 60, 31, 38, 24, 86, 40, 16, 69, 7, 38, 55, 27, 28, 10, 51, 29, 28, 30, 12 ]

average = sum(grades_from_excel) / len(grades_from_excel)

print "For this exam, the average is", average


# Number-1 of parts of the pie chart. 10 is good
nb = 10

# We create a dictionnary, to count how many students were in each range [0%, 10%), [10%, 20%) etc
count = {nb*i: 0 for i in range(0, nb+1)}
# This uses dict comprehension, cf. https://www.python.org/dev/peps/pep-0274/ for more details


# We convert the grades into integers 0, 1, .., 10
grades = [nb*(floor(g/nb)) for g in grades_from_excel]
# This uses list comprehension, cf. https://www.python.org/dev/peps/pep-0202/
# Or https://en.wikipedia.org/wiki/List_comprehension#Python for one simple example


# And we count
for g in grades:
    count[g] += 1  # One more student had his grade in THAT range [g, g+1)

# We display the result of this step
# The order of this for loop is random, it will NOT print [0, 10), [10, 20) etc.
for g, c in count.items():
    print c, "students got a mark between", g, "% and", g+1, "%."

    # We remove ranges with no students, # As we do not want to print empty parts on the pie chart
    if c == 0:
        count.pop(g)


# I want the parts in the pie chart to be sorted, so let us sort the keys
keys = count.keys()
keys.sort()  # This method will sort IN-PLACE the list keys

# Create the labels, again with list comprehension
# And string formatting (see https://docs.python.org/2/library/stdtypes.html#str.format)
labels = ["{} marks in [{}%, {}%]".format(count[k], k, k+10) for k in keys]


# Create the data that will be plotted
x = [count[k] for k in keys]  # one more example of list comprehension !


# Create the range of colors (grey) used for the pie chart
from matplotlib.colors import ColorConverter
cc = ColorConverter()  # technical boring step
# More details on http://matplotlib.org/api/colors_api.html#matplotlib.colors.ColorConverter.to_rgb

colors = [cc.to_rgb(str(1.0-k/100.0)) for k in keys]  # this will produce a scale of gray


## Now we (finally) make the pie chart
#fig = figure(1)
#
## With a title
#title("Pie chart representation of the performance for CS101 First Mid Term Exam")
#
#pie(x,  # Using the list x as data
#    labels=labels,  # The labels we created
#    autopct="%i%%",  # A format string to be used for writing a label inside each wedge
#    explode=[0.1]*len(x),  # Explode the pie chart
#    colors=colors)  # And we use that range of gray colors
#
## Finally, we save the plot to a PNG image
#savefig("Pie_chart_representation_of_the_performance_for_CS101_First_Mid_Term_Exam.png")
## And a PDF document
#savefig("Pie_chart_representation_of_the_performance_for_CS101_First_Mid_Term_Exam.pdf")
#
#print "We are done plotting, and two files has been produced in the current directory (one .png and one .pdf)."
#print "An exercise for you can be to produce a colored drawing (change the way the colors list is created)."


fig = figure(2)
title("Histogram of the performance for CS101 Second Mid Term Exam")
xlabel(r"Grades between 0% and 100%.")
ylabel("Number of students with that grades.")
ylim(0, max(x)+1)

bar(keys,  # this is the data for x axis
    x, # this is the data for y axis
    color=colors, width=nb*0.9)
for xpixel, ypixel in zip(keys, x):
    text(xpixel+nb/2, ypixel+0.05, '%i students' % ypixel, ha='center', va='bottom')

# Draw a vertical line for the average
plot([average, average], [0, max(x)+1], 'r-', linewidth=4)
text(average+2, max(x)-5, "Average ({:g})".format(round(average, 2)))


# Finally, we save the second plot to a PNG image
savefig("Histogram_of_the_performance_for_CS101__Second_Mid_Term_Exam.png")
# And a PDF document
savefig("Histogram_of_the_performance_for_CS101__First_Mid_Term_Exam.pdf")

