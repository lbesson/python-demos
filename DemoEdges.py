#! /usr/bin/env python
# -*- coding: utf-8 -*-
"""
Demo of the skimage Python toolbox, for image processing and computer vision.
Here we are demonstrating how to detect edges of an image.

@date: Fri Jan 30 16:45:58 2015.
@author: Lilian Besson for CS101 course at Mahindra Ecole Centrale 2015.
@licence: GNU Public License version 3.
"""

from pylab import *

from skimage.color import rgb2gray
from skimage.data import camera, lena
from skimage.filter import roberts, sobel

# With Colors
fig1 = figure()
imshow(lena(), cmap=cm.gray)
title('Colored Lena')
axis('off')
show()
savefig('Colored_Lena.png')

# With gray scale
fig2 = figure()
image = rgb2gray(lena())
#image = rgb2gray(camera())

imshow(image, cmap=cm.gray)
title('Gray Lena')
axis('off')
show()
savefig('Gray_Lena.png')

# Detecting edges
edge_roberts = roberts(image)
edge_sobel = sobel(image)

fig3, (ax0, ax1) = subplots(ncols=2)

ax0.imshow(edge_roberts, cmap=cm.gray)
ax0.set_title('Roberts Edge Detection')
ax0.axis('off')

ax1.imshow(edge_sobel, cmap=cm.gray)
ax1.set_title('Sobel Edge Detection')
ax1.axis('off')

show()
savefig('Edges_Lena.png')
