#! /usr/bin/env python
# -*- coding: utf-8 -*-
"""
Demonstration of the scikit-monaco package.

"Scikit-monaco is a toolkit for Monte Carlo integration. It is written in Cython for efficiency and includes parallelism to take advantage of multi-core processors."


@reference: http://scikit-monaco.readthedocs.org
@date: Thu Apr 23 14:03:57 2015.
@author: Lilian Besson for CS101 course at Mahindra Ecole Centrale 2015.
@licence: MIT Licence (http://lbesson.mit-license.org).
"""

import math
import numpy as np
from skmonaco import mcquad

print "\nThis demo script is based on the beginner tutorial at:"
print "http://scikit-monaco.readthedocs.org/en/latest/tutorials/getting_started.html\n"

# %% First example
print "\n# Example 1:"
print r"Computing the double integral \int_0^1 \int_0^1 x \cdot y \, dx dy:"

N = 100000
f = lambda x_y: x_y[0]*x_y[1]
integral, error = mcquad(f, # integrand
       xl=[0.,0.],xu=[1.,1.], # lower and upper limits of integration
       npoints=N # number of points
       )

print "skmonaco.mcquad found an approximation for the integral to be {:g}.".format(integral)
print "With an estimate of the error of {:%}.".format(error)
print "The exact value is 1/2."


# %% Second example
print "\n# Example 2:"
print "We want to evaluate the product of two Gaussians with exponential factors alpha and beta:"

f = lambda x_y,alpha,beta: np.exp(-alpha*x_y[0]**2)*np.exp(-beta*x_y[1]**2)

alpha = 1.0
beta = 2.0
print "alpha = {:g} and beta = {:g}.".format(alpha, beta)

integral, error =  mcquad(f,xl=[0.,0.],xu=[1.,1.],npoints=100000,args=(alpha,beta))
#(0.44650031245386379, 0.00079929285076240579)

print "skmonaco.mcquad found an approximation for the integral to be {:g}.".format(integral)
print "With an estimate of the error of {:%}.".format(error)


# %% Third example
print "\n# Example 3:"
print "Demo of multi-processor execution."
print r"Computing the double integral \int_0^1 \int_0^1 \cos(x) \cdot \sin(y) \, dx dy:"

N = int(1e6)
f = lambda (x, y): math.sin(x)*math.cos(y)

#print "With one core:"
#%timeit mcquad(f, xl=[0.,0.], xu=[1.,1.], npoints=N, nprocs=1)
# 1 loops, best of 3: 2.46 s per loop

#print "With two cores:"
#%timeit mcquad(f, xl=[0.,0.], xu=[1.,1.],npoints=N, nprocs=2)
# Fails:
# PicklingError: Can't pickle <function fun at 0x0000000015252D68>: it's not found as skmonaco.mp.fun


#print "With four cores:"
#integral, error = mcquad(f, xl=[0.,0.], xu=[1.,1.], npoints=N, nprocs=4)

integral, error = mcquad(f, xl=[0.,0.], xu=[1.,1.], npoints=N)

print "skmonaco.mcquad found an approximation for the integral to be {:g}.".format(integral)
print "With an estimate of the error of {:%}.".format(error)


# %% Forth example
print "\n# Example 4:"
print "Demo of integration on a more complex domain."

print " sqrt(x^2 + y^2) >= 2"
print " sqrt(x^2 + y^2) <= 3"
print " x >= 1"
print " y >= -2"

def f(x_y):
    x, y = x_y  # tuple unpacking
    return y**2

def g(x_y):
    """ The integrand, which take into account the domain."""
    x,y = x_y
    r = np.sqrt(x**2 + y**2)
    if r >= 2. and r <= 3. and x >= 1. and y >= -2.:
        # (x, y) in correct volume
        return f(x_y)
    else:
        return 0.

integral, error = mcquad(g, npoints=100000, xl=[1.,-2.], xu=[3.,3.], nprocs=1)

print "skmonaco.mcquad found an approximation for the integral to be {:g}.".format(integral)
print "With an estimate of the error of {:%}.".format(error)


# %% Done
print "\n\nA detailed overview of the module features can be found at:"
print "http://scikit-monaco.readthedocs.org/en/latest/api.html"

# End