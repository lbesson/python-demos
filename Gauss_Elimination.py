#! /usr/bin/env python
# -*- coding: utf-8 -*-
"""
Basic implementation of the Gauss Elimination process on Rn or Cn.

Matrices are represented as list of line vectors (list of lists).

Reference for notations and concepts is https://en.wikipedia.org/wiki/Gaussian_elimination#Definitions_and_example_of_algorithm

@date: Tue Mar 24 16:19:05 2015.
@author: Lilian Besson for CS101 course at Mahindra Ecole Centrale 2015.
@licence: MIT Licence (http://lbesson.mit-license.org).
"""

# %% 3 auxiliary functions used by the Gauss elimination process

def argmax(indexes, array):
    """ Compute the index i in indexes such that the abs(array[i]) is the bigger."""
    besti, bestvalue = None, None
    for i in indexes:
        if array[i] > bestvalue:
            besti = i
    if not besti:
        raise ValueError("argmax() arg is a non-valid sequence.")
    return besti


def swap_rows(a, i, k):
    """ Change *in-place* the i-th and k-th *columns* of the matrix a."""
    for j in xrange(len(a)):
        a[j][i], a[j][k] = a[j][k], a[j][i]


def prod(iterator):
    """ Compute the product of the values in the iterator.
    Empty product is 1."""
    p = 1
    for x in iterator:
        p *= x
    return p


# %% Gauss Elimination process

def gauss_elimination(a, verb=False, det=False):
    """ Basic implementation of the Gauss Elimination process.

    - It uses elementary operations on lines: L_i' <-- L_i - gamma * L_k.
    - The function will raise a ValueError if the matrix a is singular.
    - If verb is True, some details are printed at each steps of the algorithm.
    - If det is True, the determinant of the matrix is returned additionnally to its row echelon form (experimental).
    - Can swap two columns in order to select the bigger pivot (increases the numerical stability).

    Reference: https://en.wikipedia.org/wiki/Gaussian_elimination#Definitions_and_example_of_algorithm
    """
    # Number of lines and columns
    n = len(a)
    m = len(a[0])
    # Check that the matrix is a good matrix
    assert all( [ m == len(a[i]) for i in xrange(n) ] )

    # Fresh copy of the matrix a
    c = [ [ a[i][j] for j in xrange(m) ] for i in xrange(n) ]

    # assert id(a) != id(c)
    # assert all( [ id(a[i]) != id(c[i]) for i in xrange(n) ] )

    d = 1
    # https://en.wikipedia.org/wiki/Gaussian_elimination#Computing_determinants

    for k in xrange(min(n, m)):
        if verb:
            print "\nTrying to find the k-th pivot:"
        i_max = argmax( range(k, m), [ abs(c[k][i]) for i in xrange(m) ] )
        assert k <= i_max < m

        if c[k][i_max] == 0 and k < (min(n, m) - 1):
            raise ValueError("gauss_elimination() called on a singular matrix.")

        swap_rows(c, i_max, k)
        # Swapping two rows multiplies the determinant by -1
        if i_max != k:
            if verb:
                print "I swapped two different line, the determinant will be multiplied by -1."
            d *= -1

        if k >= (min(n, m) - 1):
            if verb:
                print "For the last line, we swapped the {i_max}-th and {k}-th rows, but nothing else.".format(i_max=i_max, k=k)
            break

        if verb:
            print "Gauss Elimination: using the {k}th line (L_{k} = {l}).\n  We use {pivot} as a pivot.".format(k=k, l=c[k], pivot=c[k][k])

        # Do for all lines below pivot:
        for i in xrange(k+1, n):
            gamma = float(c[i][k]) / float(c[k][k])
            # Multiplying a row by a nonzero scalar multiplies the determinant by the same scalar
#            if gamma != 0:
#                d *= gamma  # FIXME
#                if verb:
#                    print "I multiplied a row by a nonzero scalar", gamma, "so the determinant will be multiplied by the same scalar."
            if verb:
                print " Operation L_{i}' <-- L_{i} - gamma * L_{k}".format(i=i, k=k)
                print "  with gamma =", gamma
                print "  with old L_{i} = {l}".format(i=i, l=c[i])
            # Do for all remaining elements in current line:
            for j in xrange(k+1, m):
                c[i][j] -= gamma * c[k][j]
                # We convert to integer if possible, it is prettier :)
                # if int(c[i][j]) == c[i][j]:
                #     c[i][j] = int(c[i][j])
            #  Fill lower triangular matrix with zeros (because gamma is chosed like that):
            c[i][k] = 0
            if verb:
                print "  with new L_{i}' = {l}".format(i=i, l=c[i])
    # Done, return the new matrix c
    if det:
        return c, d * prod( [ c[i][i] for i in xrange(len(c)) ] )
    else:
        return c


# %% Applications of the Gauss elimination process
# Reference is https://en.wikipedia.org/wiki/Gaussian_elimination

def rank(a, verb=True):
    """ Uses the Gauss elimination process to compute the rank of the matrix, by simply counting the number of non-zero elements on the diagonal of the echelon form."""
    n = len(a)
    m = len(a[0])
    c = gauss_elimination(a, verb=verb)
    return sum( [ c[i][i] != 0 for i in xrange(min(n, m)) ] )


def det(a, verb=False):
    """ Uses the Gauss elimination process to compute the determinant of the matrix.
    Because it depends of the number of elementary operations performed in the Gauss method, I add to modify the gauss_elimination function..."""
    c, d = gauss_elimination(a, verb=verb, det=True)
    if int(d) == d:
        d = int(d)
    return d


# %% Test the Gauss Elimination process on any matrix
def test_gauss(m):
    print "\n\nFor the matrix m =", m, "we apply the Gauss Elimination process."
    c = gauss_elimination(m, verb=True)
    print "\nThe Gauss Elimination reduction matrix is", c
    print "Now computing the rank:"
    r = rank(m, verb=False)
    print "Hence, the rank of the matrix m is", r
    print "Now computing the determinant:"
    d = det(m, verb=False)
    print "Hence, the determinant of the matrix m is", d
    return c, r, d


if __name__ == '__main__':
    m = [ [1, 2], [3, 4] ]
    test_gauss(m)
    m = [ [1, 2], [2, 4] ]
    test_gauss(m)


# %% Here are some more examples
if __name__ == '__main__':
    x_1 = [1, -1, 1, -1]
    x_2 = [5, 1, 1, 1]
    x_3 = [-3, -3, 1, -3]
    print "\n\nFor the three vectors x1 =", x_1, ", x2 =", x_2, "and x3 =", x_3

    m = [x_1, x_2, x_3]

    m2 = gauss_elimination(m, verb=True)
    basis = [ m2[i][:] for i in xrange(len(m2)) if m2[i][:] != [0]*len(m2[i]) ]

    print "With Gauss Elimination, the echelon form (ie. basis of the span) is", basis
    r = rank(m, verb=False)
    print "And the dimension of the span is", r


# %% Some random tests
from random import randint

def randmatrix(n=1, m=1, maxvalue=10):
    return [ [ randint(-maxvalue, maxvalue) for j in xrange(int(m)) ] for i in xrange(int(n)) ]


if __name__ == '__main__':
    n, m = 20, 20
    nb_test = 10
    bigrank = min(n, m)
    count_bigrank = 0

    print "\n\nLet us start", nb_test, "tests of the Gauss elimination process, on matrices of size", n, "x", m

    for l in xrange(nb_test):
        a = randmatrix(n=n, m=m)
        try:
            c, r = test_gauss(a)
            count_bigrank += (r == bigrank)
        except ValueError:
            print "The matrix m =", a, "was unfortunately singular."

    print "We did", nb_test, "tests of the Gauss elimination process, on matrices of size", n, "x", m
    print count_bigrank, "of them got a rank equal to", bigrank, "the min of n =", n, " and m =", m
