#! /usr/bin/env python
# -*- coding: utf-8 -*-

#--
f = lambda x, y: cos(x**2)
print "For that function $f(x, y) = cos(x^2)$:"
print "We want compute the double integral on the domain:\t$D = {(x, y) : 0 < x < 1, 0 < y < 1, 0 < x + y < 1}$."

#--
print "\nFirst integral is $x$, then $y$: $\int_0^1 \int_y^1 f(x, y) dx dy$"

intf_x_y = integrate(integrate(f(x, y), (x, y, 1)), (y, 0, 1))
print "But that first integral is ugly!"

#--
intf_x_y

#--
print "But we can approximate its value: {:g}.".format(float(intf_x_y))

#--
print "\n\nSecond integral is $y$, then $x$: $\int_0^1 \int_0^x f(x, y) dy dx$"

intf_y_x = integrate(integrate(f(x, y), (y, 0, x)), (x, 0, 1))

print "That second integral is simpler:"

#--
intf_y_x

#--
print "But we can approximate its value: {:g}.".format(float(intf_y_x))
print "This is a nice formal result (which was expected for the Quiz)."
