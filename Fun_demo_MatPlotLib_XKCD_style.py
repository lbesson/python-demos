#! /usr/bin/env python
# -*- coding: utf-8 -*-
"""
A fun demo of the MatPlotLib plotting module for Python, by using the XKCD style.

 - Reference is http://matplotlib.org/devdocs/examples/showcase/xkcd.html?highlight=xkcd.
 - Based on "Stove Ownership" from XKCD by Randall Monroe (http://xkcd.com/418/)
 - See more examples here : http://matplotlib.org/devdocs/examples/index.html.

@date: Fri Apr 10 20:29:05 2015.
@author: Lilian Besson for CS101 course at Mahindra Ecole Centrale 2015.
@licence: MIT Licence (http://lbesson.mit-license.org).
"""

# Usual way to import MatPlotLib and NumPy:
import matplotlib.pyplot as plt
import numpy as np

# Now the plot will be in XKCD sketch-style drawing mode
with plt.xkcd():
    # Documentation reference is at http://matplotlib.org/devdocs/api/pyplot_api.html?highlight=xkcd#matplotlib.pyplot.xkcd

    # New figure
    fig = plt.figure()

    # Customize the axes of the figure
    ax = fig.add_axes((0.1, 0.2, 0.8, 0.7))

    # Remove the ticks
    plt.xticks([])
    plt.yticks([])

    # Trick the limits
    ax.set_ylim([-30, 30])

    # Create the artificial data
    data = np.ones(100)

    # This part will be monotonically non-decreasing
    data[50:] += np.arange(50)

    # We annotate with a fun explanation
    plt.annotate(
        "THE DAY WE STARTED TO LEARN HOW TO\nUSE PYTHON TO DO SCIENTIFIC PLOTTING",
        xy=(50, 1), arrowprops=dict(arrowstyle='->'), xytext=(15, -20))

    # We plot the artificial data ----/
    plt.plot(data, linewidth=6, color='red')

    # x and y labels
    plt.xlabel("TIME")
    plt.ylabel("HOW EXCITING THE CS101 LECTURES ARE")

    # Add one text
    fig.text(
        0.5, 0.05,
        "First demo of MatPlotLib (XKCD style)",
        ha='center')

    # Done, we show the plot (automatic in interactive mode)
    # plt.show()

    # and we can save it, to include it in my slides
    plt.savefig("Fun_demo_MatPlotLib_XKCD_style.png")

# Done !
