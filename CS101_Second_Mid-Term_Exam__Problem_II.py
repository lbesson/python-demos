#! /usr/bin/env python
# -*- coding: utf-8 -*-
"""
Commented solution for problem II for the CS101 second mid-term Exam.

The program given in the paper has been modified slightly (to not be too ugly and to follow the usual typing convention of Python).

All the comments are here to help you understand, and none of them were required during the exam.

Obviously, all these functions can be written in *many* different way, here is only one proposal (the simpler / more elegant).

Examples and tests are included in the end of the program.


@date: Thu Mar 26 22:04:54 2015.
@author: Lilian Besson for CS101 course at Mahindra Ecole Centrale 2015.
@licence: MIT Licence (http://lbesson.mit-license.org).
"""


# %% Question Q.II.1)

def quick_exponentation(a, b):
    """ Compute a (an integer/float) to the power b quickly (b has to be an integer!).

    - Time complexity : O(log(b)) for an integer number a and a power b.
    - Raise a ValueError exception if b < 0.
    """
    assert isinstance(b, int)
    if b == 0:
        return 1
    elif b == 1:
        return a
    elif b % 2 == 0 and b > 0:
        # a^(2k) = ((a^2) ^ k)
        return quick_exponentation(a**2, b/2)
    # The other case was not included in the exam, but is not hard:
    elif b % 2 == 1 and b > 0:
        # a^(2k+1) = a * ((a^2) ^ k)
        return a * quick_exponentation(a**2, (b-1)/2)
        # Remark: this case is not tail recursive
        # We should have used an accumulator
    else:
        raise ValueError("quick_exponentation() invalid value for the power b = {b}.".format(b=b))


# %% Question Q.II.2)

# Obviously, the function has to be defined with two arguments
# a, b (different names), otherwise it fails, by raising
# SyntaxError: duplicate argument 'a' in function definition

def sqr_mat_mult(a, b):
    """ Compute the matrix a * b for two square matrices a and b of same size (n, n).

    - Time complexity : O(n^3) for two matrices a and b of size (n, n).
    - Remark: the Strassen algorithm can be used to obtain a complexity of O(n^log2(7)), sligtly better than n^3 = n^log2(8). More on https://en.wikipedia.org/wiki/Strassen_algorithm#Asymptotic_complexity.
    """
    n = len(a)  # Useless to ask it as an argument !

    # Check that a and b have the same size and are square
    assert n == len(b)
    assert all( [ len(a[i]) == n for i in xrange(n) ] )
    assert all( [ len(a[i]) == n for i in xrange(n) ] )
    # All this was NOT asked in the exam

    return [ [ sum([ a[i][k] * b[k][j]  for k in xrange(n) ])  for j in xrange(n) ]  for i in xrange(n) ]
    # As seen in class and the lab, one big list comprehension is enough


def quick_mat_exponentation(a, b):
    """ Compute a (a square matrix) to the power b quickly (b has to be an integer!).

    - Time complexity : O(n^3 log(b)) for a matrix a of size (n, n) and a power b.
    - Raise a ValueError exception if b < 0.
    """
    assert isinstance(b, int)
    if b == 0:
        return 1
    elif b == 1:
        return a
    elif b % 2 == 0 and b > 0:
        p = sqr_mat_mult(a, a)
        # a^(2k) = ((p) ^ k)
        return quick_mat_exponentation(p, b/2)
    # The other case was not included in the exam, but is not hard:
    elif b % 2 == 1 and b > 0:
        p = sqr_mat_mult(a, a)
        # a^(2k+1) = a * ((a^2) ^ k)
        return sqr_mat_mult(a, quick_mat_exponentation(p, (b-1)/2))
        # Remark: this case is not tail recursive
        # We should have used an accumulator
    else:
        raise ValueError("quick_mat_exponentation() invalid value for the power b = {b}.".format(b=b))



# %% We test all these functions
if __name__ == '__main__':
    assert quick_exponentation(2, 16) == 2**16
    from random import randint
    for test in xrange(1000):
        a = randint(-10, 10)
        b = randint(1, 10000)
        assert quick_exponentation(a, b) == a**b
    print "We successfully did 1000 tests of quick a**b with b upto 10000."
    for test in xrange(10):
        a = [ [ randint(-10, 10) for j in xrange(3) ] for i in xrange(3) ]
        print "For the matrix a =", a
        b = randint(1, 64)
        print "And the power b =", b
        # Not really possible to check it...
        print "a ** b is", quick_mat_exponentation(a, b)