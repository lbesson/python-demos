#! /usr/bin/env python
# -*- coding: utf-8 -*-
"""
Quick exponentation in Python (for int/float numbers), tailrec or not.

@date: Thu Feb 05 23:36:50 2015.
@author: Lilian Besson for CS101 course at Mahindra Ecole Centrale 2015.
@licence: GNU Public License version 3.
"""

def powRec1(x, n, acc=1):
    """ Non-tail recursive implementation of the quick exponentation algorithm. """
    assert type(n) is int
    assert n >= 0
    # We deal first with special cases
    if n == 0:
        return 1
    elif n == 1:
        return x
    # And then the magic starts
    elif (n % 2) == 0:
        k = n / 2
        return powRec(x, k) ** 2
    elif (n % 2) == 1:
        k = n / 2
        return x * (powRec(x, k) ** 2)
        # Here we are NOT Tail Recursive, so the function will be limited to ~ 10000 subcalls of itself
        # ... Which is still WAY ENOUGH because the number of calls is 2**n.
        # And 2**100000 is quick HUGE (approximately 10 to the power 3000 !).
        # See this https://docs.python.org/2/library/sys.html#sys.getrecursionlimit
    else:
    # This case cannot happen
        raise ValueError("powRec called on wrong arguments x = {}, n = {}.".format(x, n))


def powRecAcc(x, n, acc=1):
    """ Tail recursive implementation of the quick exponentation algorithm. """
    assert type(n) is int
    assert n >= 0
    # We deal first with special cases
    if n == 0:
        return acc
    elif n == 1:
        return x * acc
    # And then the magic starts
    elif (n % 2) == 0:
        k = n / 2
        return powRecAcc(x ** 2, k, acc)
    elif (n % 2) == 1:
        k = n / 2
        return powRecAcc(x ** 2, k, x * acc)
        # Thanks to this accumulator, we are now Tail Recursive (https://en.wikipedia.org/wiki/Tail_call)
        # And therefore, powRec can be called with basically any value of n
    else:
    # This case cannot happen
        raise ValueError("powRecAcc called on wrong arguments x = {}, n = {}.".format(x, n))


def powRec(x, n):
    """ Compute x**n with log_2(n) square operations and multiplications."""
    return powRecAcc(x, n, acc=1)


if __name__ == "__main__":
    import random
    N = 5000
    for i in xrange(N):
        x = random.randint(0, N)
        n = random.randint(0, N)
        assert x**n == powRec(x, n)
        print "{}th test: for x={}, n={}, we checked our function.".format(i, x, n)

