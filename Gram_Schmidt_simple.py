#! /usr/bin/env python
# -*- coding: utf-8 -*-
"""
Simple implementation of the Gram-Schmidt process on Rn.

Matrices are represented as list of line vectors (list of lists).

@date: Tue Mar 24 16:19:05 2015.
@author: Lilian Besson for CS101 course at Mahindra Ecole Centrale 2015.
@licence: MIT Licence (http://lbesson.mit-license.org).
"""


# Vectors operations <u, v>, ||u|| and proj_u(v)

def dot_vectors(a, b):
    """ Dot product of the two vectors a and b (real numbers ONLY)."""
    assert len(a) == len(b)
    return sum( [ a[i]*b[i] for i in xrange(len(a))] )
    # FIXME change it if the vectors contain complex coordinates?


def norm_square(u):
    """ Shortcut for the square of the norm of the vector u : ||u||^2 = <u, u>."""
    return dot_vectors(u, u)


def vect_const_multi(a, c):
    """ Multiply the vector a by the constant c."""
    return [ c*a[i] for i in xrange(len(a))]


def proj(u, v):
    """ Projection of the vector v into the vector u (proj_u(v) as written on Wikipedia)."""
    nsqu = norm_square(u)
    if nsqu == 0:
        return [0] * len(u)
    else:
        udotu = float(nsqu)
        return vect_const_multi(u, dot_vectors(u, v) / udotu)


# %% Gram-Schmidt process

def gram_schmidt(a, verb=False):
    """ Basic implementation of the Gram-Schmidt process, in the easy case of Rn with the usual <.,.>.

    Reference for notations, concept and proof is https://en.wikipedia.org/wiki/Gram%E2%80%93Schmidt_process.

    If verb is True, some details are printed at each steps of the algorithm.
    """
    # Number of lines and columns
    n = len(a)
    m = len(a[0])
    # Checking that the matrix is a good matrix (not necessary)
    assert all( [ m == len(a[i]) for i in xrange(n) ] )

    # Getting a fresh copy of the matrix a !
    c = [ [ a[i][j] for j in xrange(m) ] for i in xrange(n) ]
    # c[0] = a[0][:]

    # Start the first loop
    for i in xrange(1, n):
        if verb:
            print "\nGram-Schmidt: using the {i}th vector (v_{i} = {l}) to compute the new {i}th vector u_{i}.".format(i=i, l=c[i])
        p = proj(c[i-1], c[i])
        c[i] = [ c[i][j] - p[j] for j in xrange(m) ]
        # Now u_{i-1} and u_i are orthogonal !
    return c


def rank(a):
    """ Use the Gram-Schmidt process to compute the rank of the matrix, by simply computing the number of non-zero vectors in the orthogonal basis obtained at the end."""
    n = len(a)
    m = len(a[0])
    c = gram_schmidt(a, verb=False)
    epsilon = 1e-4
    # We are using float numbers, so a tolerance threshold is mandatory
    return sum( [ norm_square(c[i]) > epsilon for i in xrange(min(n, m)) ] )


# %% Test the Gram-Schmidt process on easy matrices
def test_gram(m):
    print "\n\nFor the matrix m =", m, "we apply the Gram-Schmidt process."
    c = gram_schmidt(m, verb=True)
    print "The Gram-Schmidt reduction matrix is", c
    r = rank(m)
    print "Hence, the rank of the matrix m is", r


# %% Example coming from https://en.wikipedia.org/wiki/Gram%E2%80%93Schmidt_process#Example
if __name__ == '__main__':
    m = [ [3, 1], [2, 2]]
    test_gram(m)


# %% Two other examples
if __name__ == '__main__':
    m = [ [1, 2], [3, 4] ]
    test_gram(m)
    m = [ [1, 2], [2, 4] ]
    test_gram(m)


# %% Example from one student
if __name__ == '__main__':
    x_1 = [1, -1, 1, -1]
    x_2 = [5, 1, 1, 1]
    x_3 = [-3, -3, 1, -3]
    print "For the three vectors x1 =", x_1, ", x2 =", x_2, "and x3 =", x_3

    m = [x_1, x_2, x_3]

    basis = gram_schmidt(m, verb=True)
    print "With Gram-Schmidt, the orthogonal set is" , basis
