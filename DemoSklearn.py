#! /usr/bin/env python
# -*- coding: utf-8 -*-
"""
Demo of the sklearn Python toolbox, for machine learning.
TODO: find one nice example.

Idea: prediciting grades for the final exam based on the grades on the first two mid term (for MA101) ?

Strategy:

 1. get the data
 2. specify what is the goal of the computation
 3. chose the best learner/classifier to be used (from sklearn)
 4. separate dataset in learning/testing set (50%, 50%) randomly
 5. test and improve as long as performance is bad


@url: http://scikit-learn.org/stable/tutorial/basic/tutorial.html#introduction
@date: Fri Jan 30 16:45:58 2015.
@author: Lilian Besson for CS101 course at Mahindra Ecole Centrale 2015.
@licence: GNU Public License version 3.
"""

from pylab import *
from numpy.random import permutation

from sklearn import svm
from sklearn import datasets


# %% First demo
print "First demo: with the digits database."
digits = datasets.load_digits()
X, y = digits.data, digits.target
#X, y = permutation([X, y])

# I want the demo to be different for each execution
#shuffle(digits)

n_samples, n_features = X.shape
print "The digits database has been loaded, with {} samples corresponding to a vector of size {}, each being classified as one digit from 0 to 9.".format(n_samples, n_features)

# Learning and predicting with a SVM
clf = svm.SVC(gamma=0.001, C=100.)

# Here we make the classifier learn
clf.fit(X[:-1], y[:-1])

target = X[-1]
image_target = digits.images[-1]

#print "The target image is:", target, "and the classifier will now be used to predict its value."
predicted_target = clf.predict(target)

fig = figure()
title("Example of a digit: its predicted value is {}".format(predicted_target))
imshow(image_target, cmap='gray', interpolation='none')
show()

print "Done with that demo."


# %% Second demo
print "First demo: with the iris database."

iris = datasets.load_iris()
X, y = iris.data, iris.target
#X, y = permutation([X, y])

n_samples, n_features = X.shape
print "The iris database has been loaded, with {} samples corresponding to a vector of size {}.".format(n_samples, n_features)

# Learning and predicting with a SVM
clf = svm.SVC()

# Here we make the classifier learn
clf.fit(X[:-1], y[:-1])

import pickle
print "\nFirst pickling: to and from a variable..."
s = pickle.dumps(clf)
clf2 = pickle.loads(s)

target_predicted = clf2.predict(X[0])[0]
target_real = y[0]
print "For the first vector, labeled as {}, the predicted label is {}.".format(target_real, target_predicted)

# Try with joblib sklearn's module
from sklearn.externals import joblib
filename = 'iris_clf.pkl'
print "\nSecond pickling: to and from a file ({})...".format(filename)
joblib.dump(clf, filename)
clf = joblib.load(filename)

target_predicted = clf.predict(X[0])[0]
target_real = y[0]
print "For the first vector, labeled as {}, the predicted label is {}.".format(target_real, target_predicted)

print "Done with second that demo."
