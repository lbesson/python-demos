#! /usr/bin/env python
# -*- coding: utf-8 -*-
"""
Demo of basics linear algebra operations with Python using the numpy/sympy modules.

Documentation is on http://docs.scipy.org/doc/numpy/reference/routines.linalg.html

@date: Thu Feb 26 12:21:36 2015.
@author: Lilian Besson for CS101 course at Mahindra Ecole Centrale 2015.
@licence: GNU Public License version 3.
"""

import numpy as np
from numpy.linalg import *

# %% Problem 8
family8 = [
    [1, 2, -1, 3],
    [1, 0, 0, 2],
    [2, 8, -4, 8],
    [1, 1, 1, 1]
]

# This step is useless, but nice: we convert these 4 vectors to a 4x4 matrix called M
M = np.matrix(family8)

print "Tutorial sheet 4 (MA102), Problem 8 : the space spanned by the", len(family8), "vectors ", family8, "is of dimension :", matrix_rank(M)

# This function matrix_rank compute the rank of the matrix M
# You will study in detail this concept of rank

# The basic definition is exactly what we want :
# If M = [x1, .., xn], its rank is the dimension of span(x1, .., xn)


# %% Problem 9
family9 = [
    [1, 2, -1, 3],
    [1, 0, 0, 2],
    [2, 8, -4, 8],
    [1, 1, 1, 1],
    [3, 3, 0, 6]
]

# This step is useless, but nice: we convert these 4 vectors to a 4x4 matrix called M
M = np.matrix(family9)

print "Tutorial sheet 4 (MA102), Problem 9 : the space spanned by the", len(family9), "vectors ", family9, "is of dimension :", matrix_rank(M)


# =========================================================
# %% Problem 10 (partial solution)

# %% First product :
def prod1(x, y):
    assert len(x) == len(y)
    return sum([ x[i] * abs(y[i]) for i in xrange(len(x)) ])


print "The first function is NOT an inner product, because it is:\n"
# An inner product is a linear form (additive and homogeneous), which is also symetrical and defined positive
# En francais : un product scalaire est une forme lineaire symetrique definie positive

x, y, z = [-1], [1], [-1]
yplusz = [ y[i] + z[i] for i in xrange(len(y)) ]  # [0]
print " - NOT additive (for second coordinate): x =", x, "and y =", y, "and z =", z, "produce <x, y + z> = ", prod1(x, yplusz), "which is not <x, y> + <x, z> =", prod1(x, y) + prod1(x, z)

x, y = [-1], [1]
c = -1
print " - NOT homogeneous: c =", c, "and x =", x, "and y =", y, "produce c<x, y> = ", c*prod1(x, y), "which is not <x, c*y> =", prod1(x, [c*yi for yi in y])

x, y = [-1], [1]
print " - NOT symetrical: x =", x, "and y =", y, "produce <x, y> = ", prod1(x, y), "which is not <y, x> =", prod1(y, x)

x = [-1, 1]
print " - NOT well defined: x =", x, "produces <x, x> = ", prod1(x, x), "but x is not [0, 0]"
# FIXME translation

x = [-1]
print " - NOT positive: x =", x, "produce <x, x> = ", prod1(x, x)


# %% Second product :
def prod2(x, y):
    assert len(x) == len(y)
    return abs(sum([ x[i] * y[i] for i in xrange(len(x)) ]))
#    return abs(dot(x, y))  # dot function from numpy


print "The second function is NOT inner product, because it is:\n"
# An inner product is a linear form (additive and homogeneous), which is also symetrical and defined positive
# En francais : un product scalaire est une forme lineaire symetrique definie positive

x, y, z = [1], [1], [-1]
yplusz = [ y[i] + z[i] for i in xrange(len(y)) ]  # [0]
print " - NOT additive (for both coordinates): x =", x, "and y =", y, "and z =", z, "produce <x, y + z> = ", prod2(x, yplusz), "which is not <x, y> + <x, z> =", prod2(x, y) + prod2(x, z)

x, y = [1], [1]
c = -1
print " - NOT homogeneous: c =", c, "and x =", x, "and y =", y, "produce c<x, y> = ", c*prod2(x, y), "which is not <x, c*y> =", prod2(x, [c*yi for yi in y])

print " - is symetrical (prove it)."

print " - is well defined (prove it)."

print " - is positive (prove it)."


# %% Third product :
def prod3(x, y):
    assert len(x) == len(y)
    return sum(x)*sum(y)


print "The third function is NOT an inner product, because it is:\n"
# TODO


# %% Forth product :
def prod4(x, y):
    assert len(x) == len(y)
    from math import sqrt
    return sqrt(sum([ (x[i]**2) * (y[i]**2) for i in xrange(len(x)) ]))


print "The forth function is NOT an inner product, because it is:\n"
# TODO


# %% Fifth product :
def prod5(x, y):
    assert len(x) == len(y)
    return sum([ (x[i] + y[i])**2 for i in xrange(len(x)) ]) \
        - sum([ x[i]**2 for i in xrange(len(x)) ]) \
        - sum([ y[i]**2 for i in xrange(len(x)) ])


print "The fifth function is an inner product, and you can prove it: <x, y> = .. = 2 sum(xi yi).\n"


