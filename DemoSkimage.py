#! /usr/bin/env python
# -*- coding: utf-8 -*-
"""
Demo of the skimage Python toolbox, for image processing and computer vision.

@date: Fri Jan 30 16:45:58 2015.
@author: Lilian Besson for CS101 course at Mahindra Ecole Centrale 2015.
@licence: GNU Public License version 3.
"""

# %%
from pylab import *
# import skimage

from skimage import data
from skimage.filter.rank import entropy
from skimage.morphology import disk
from skimage.util import img_as_ubyte

# image = img_as_ubyte(data.camera())

#for ubyte in [data.camera(), data.moon(), data.coins(), data.checkerboard()]:

for i, name in enumerate(['camera', 'moon', 'coins', 'checkerboard']):
    exec('ubyte = data.'+name+'()')  # FIXME: durty
    image = img_as_ubyte(ubyte)

    fig, (ax0, ax1) = subplots(ncols=2, figsize=(10, 4))
    title('Figure number {}'.format(i))

    img0 = ax0.imshow(image, cmap='gray')
    ax0.set_title('Image ('+name+')')
    ax0.axis('off')
    fig.colorbar(img0, ax=ax0)

    img1 = ax1.imshow(entropy(image, disk(5)), cmap='jet')
    ax1.set_title('Entropy (for '+name+')')
    ax1.axis('off')
    fig.colorbar(img1, ax=ax1)

    show()
    savefig(name+'_entropy.svg')
