#! /usr/bin/env python
# -*- coding: utf-8 -*-
"""
Empty description.

@date: Fri Feb 27 20:39:18 2015.
@author: Lilian Besson for CS101 course at Mahindra Ecole Centrale 2015.
@licence: GNU Public License version 3.
"""

import pygame, sys

running = True

deepblue = (26,0,255)
mintcream = (254,255,250)
pygame.init()
size = (500,500)
surface = pygame.display.set_mode(size)
surface.fill(deepblue)
position = (250,250)
radius = 50
linewidth = 2
pygame.draw.circle(surface, mintcream, position, radius, linewidth)
pygame.display.flip()

while running:
    event = pygame.event.wait()
    if event.type == pygame.QUIT:
        running = False
pygame.quit()
