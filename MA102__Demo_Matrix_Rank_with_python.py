#! /usr/bin/env python
# -*- coding: utf-8 -*-
"""
Demo of how to compute the dimension of the space spanned by some vectors, using one basic linear algebra operation with Python using the NumPy module.

Documentation is on http://docs.scipy.org/doc/numpy/reference/routines.linalg.html

@date: Thu Feb 26 12:21:36 2015.
@author: Lilian Besson for CS101 course at Mahindra Ecole Centrale 2015.
@licence: GNU Public License version 3.
"""

import numpy
from numpy.linalg import matrix_rank

# %% Problem 8
family8 = [
    [1, 2, -1, 3],
    [1, 0, 0, 2],
    [2, 8, -4, 8],
    [1, 1, 1, 1]
]

# This step is useless, but nice: we convert these 4 vectors to a 4x4 matrix called M
M = numpy.matrix(family8)

print "Tutorial sheet 4 (MA102), Problem 8 : the space spanned by the", len(family8), "vectors ", family8, "is of dimension :", matrix_rank(M)

# This function matrix_rank compute the rank of the matrix M
# You will study in detail this concept of rank

# The basic definition is exactly what we want :
# If M = [x1, .., xn], its rank is the dimension of span(x1, .., xn)


# %% Problem 9
family9 = [
    [1, 2, -1, 3],
    [1, 0, 0, 2],
    [2, 8, -4, 8],
    [1, 1, 1, 1],
    [3, 3, 0, 6]
]

# This step is useless, but nice: we convert these 4 vectors to a 4x4 matrix called M
M = numpy.matrix(family9)

print "Tutorial sheet 4 (MA102), Problem 9 : the space spanned by the", len(family9), "vectors ", family9, "is of dimension :", matrix_rank(M)
