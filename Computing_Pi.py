#! /usr/bin/env python
# -*- coding: utf-8 -*-
"""
Several methods for computing the π number upto a certain precision.

References:
===========

 - https://en.wikipedia.org/wiki/Pi#Modern_quest_for_more_digits
 - http://bellard.org/pi/ by Francois Bellard (and http://bellard.org/pi/pi_n2/pi_n2.html)
 - http://thelivingpearl.com/2013/05/28/computing-pi-with-python/ (good code, but the computed accuracy are bullshit, as he is comparing to math.pi which is only correct up to the 17th digits...)


First 100 digits
================
π is 3.1415926535897932384626433832795028841971693993751058209749445923078164062862089986280348253421170679 when computed to the first 100 digits.


@date: Wed Mar 04 17:42:18 2015.
@author: Lilian Besson for CS101 course at Mahindra Ecole Centrale 2015.
@licence: MIT Licence (http://lbesson.mit-license.org).
"""

import math
from decimal import *

print "The first three approximations of π are:"
print " π ~= 3.14 (two first digits)."
print " π ~= 22/7 = {} (two first digits).".format(22.0 / 7.0)
print " π ~= 355/113 = {} (six first digits).".format(355.0 / 113.0)


# %% First method: using math.pi
def mathpi():
    from math import pi
    return pi

print "First method: using math.pi gives π ~= {:.17f} (17 digits are displayed here).".format(mathpi())

# With 100 first digits
bigpi = Decimal('3.1415926535897932384626433832795028841971693993751058209749445923078164062862089986280348253421170679')

print "The first 100 digits of π are {}.".format(bigpi)


# %% Lazy method with SymPy and its mpmath module
# Mpmath is a Python library for arbitrary-precision floating-point arithmetic.

# Works really fine upto 1000000 digits (56 ms)
# From 1 million digits to be printed, printing them starts to get too time consuming (Spyder might freeze)

from sympy.mpmath import mp

# http://docs.sympy.org/dev/modules/mpmath/basics.html#setting-the-precision
mp.dps = 1000  # number of digits
# Gives pi to a thousand places
#% timeit my_pi = mp.pi
my_pi = mp.pi

print "A lazy method uses SymPy and its mpmath module: π is approximatly {} (with {} digits).".format(my_pi, mp.dps)

# Saving it for further comparison of simpler methods
sympy_mpmath_pi = Decimal(str(my_pi))


# %% The Gauss–Legendre iterative algorithm:
# From  https://en.wikipedia.org/wiki/Gauss%E2%80%93Legendre_algorithm
def gauss_legendre_1(max_step):
    print "Computing π with the Gauss–Legendre iterative algorithm upto {} steps.".format(max_step)
    a = 1.0
    b = 1.0/math.sqrt(2)
    t = 1.0/4.0
    p = 1.0

    max_step = min(5, max_step)  # useless to go further!
    for i in xrange(max_step):
        at = (a+b)/2.0
        bt = math.sqrt(a*b)
        tt = t - p*(a-at)**2
        pt = 2.0*p

        a, b, t, p = at, bt, tt, pt

    my_pi = ((a+b)**2)/(4.0*t)
    print "π is approximately: {:.15f} (as a float number, precision is limited).".format(my_pi)

    accuracy = 100*abs(math.pi - my_pi)/math.pi
    print "Accuracy % with math.pi: {:.4g}".format(accuracy)

    accuracy = 100*abs(float(sympy_mpmath_pi) - my_pi)/float(sympy_mpmath_pi)
    print "Accuracy % with sympy_mpmath_pi: {:.4g}".format(accuracy)

    return my_pi


my_pi = gauss_legendre_1(4)
print "The Gauss-Legendre algorithm is limited to a precision of 13 or 14 digits (because it uses float numbers). But it converges quickly ! (4 steps here)."


# %% The Gauss–Legendre iterative algorithm (version 2)
# with decimal numbers from Decimal module (better precision)
# More details on that library here https://docs.python.org/2/library/decimal.html
def gauss_legendre_2(max_step):
    print "Computing π with the Gauss–Legendre iterative algorithm with Decimal numbers, upto {} steps.".format(max_step)

    # trick to improve precision FIXME
    getcontext().prec = 100 + 4 * (2**max_step)
    a = Decimal(1.0)
    b = (Decimal(0.5)).sqrt()
    t = Decimal(0.25)
    p = Decimal(1.0)

    for i in xrange(max_step):
        at = (a+b)/Decimal(2.0)
        bt = (a*b).sqrt()
        tt = Decimal(t - p*(a-at)**2)
        pt = Decimal(2.0)*Decimal(p)

        a, b, t, p = at, bt, tt, pt

    my_pi = ((a+b)**2)/(Decimal(4.0)*t)
    accuracy = 100*(Decimal(math.pi)-my_pi)/my_pi

    print "π is approximately: {}.".format(my_pi)

    accuracy = 100*abs(Decimal(math.pi) - my_pi)/Decimal(math.pi)
    print "Accuracy % with math.pi: {:.4g}".format(accuracy)

    accuracy = 100*abs(sympy_mpmath_pi - my_pi)/sympy_mpmath_pi
    print "Accuracy % with sympy_mpmath_pi: {:.4g}".format(accuracy)

    return my_pi


my_pi = gauss_legendre_2(6)
print "The second implementation of the Gauss-Legendre algorithm is now better! For example, a precision of 84 digits can be obtained with only 5 steps!."
# 171 digits with 6 steps in 600 ms
# 693 digits with 8 steps in 1.09 s
# 2789 digits with 10 steps in 22.3 s


# %% Bailey-Borwein-Plouffe series
# Source: https://en.wikipedia.org/wiki/Bailey%E2%80%93Borwein%E2%80%93Plouffe_formula

def bbp(max_step):
    """ Computing an approximation of π with Bailey-Borwein-Plouffe series."""
    my_pi = Decimal(0)
    for k in xrange(max_step):
        my_pi += (Decimal(1)/(16**k))*((Decimal(4)/(8*k+1))-(Decimal(2)/(8*k+4))-(Decimal(1)/(8*k+5))-(Decimal(1)/(8*k+6)))
    return my_pi


def bailey_borwein_plouffe(precision, max_step):
    print "Computing π with the Bailey-Borwein-Plouffe method with Decimal numbers, upto {} steps and with a desired precision of {} numbers.".format(max_step, precision)

    getcontext().prec = 4 + precision  # trick to improve precision
    my_pi = bbp(max_step)
    print "π is approximately: {}.".format(my_pi)

    accuracy = 100*abs(Decimal(math.pi) - my_pi)/Decimal(math.pi)
    print "Accuracy % with math.pi: {:.4g}".format(accuracy)

    accuracy = 100*abs(sympy_mpmath_pi - my_pi)/sympy_mpmath_pi
    print "Accuracy % with sympy_mpmath_pi: {:.4g}".format(accuracy)

    return my_pi

# 1.57 s for 1000 correct digits
# A lot of time (4min 9s) for 1200 correct digits
# Not so efficient method!
my_pi = bailey_borwein_plouffe(1000, 1000)
print "The Bailey-Borwein-Plouffe method is way better, with 1.57 s for 1000 correct digits."
print "But it tends to be slow for more digits..."


# %% Bellard's formula
# Source: https://en.wikipedia.org/wiki/Bellard%27s_formula

def bellard(max_step):
    my_pi = Decimal(0)
    for k in xrange(max_step):
        my_pi += (Decimal(-1)**k/(1024**k))*( Decimal(256)/(10*k+1) + Decimal(1)/(10*k+9) - Decimal(64)/(10*k+3) - Decimal(32)/(4*k+1) - Decimal(4)/(10*k+5) - Decimal(4)/(10*k+7) -Decimal(1)/(4*k+3))
    my_pi = my_pi * Decimal(1.0/(2**6))
    return my_pi


def bellard_method(precision, max_step):
    print "Computing π with the Bellard's formula with Decimal numbers, upto {} steps and with a desired precision of {} numbers.".format(max_step, precision)

    getcontext().prec = 3 + precision  # trick to improve precision
    my_pi = bellard(max_step)
    print "π is approximately: {}.".format(my_pi)

    accuracy = 100*abs(Decimal(math.pi) - my_pi)/Decimal(math.pi)
    print "Accuracy % with math.pi: {:.4g}".format(accuracy)

    accuracy = 100*abs(sympy_mpmath_pi - my_pi)/sympy_mpmath_pi
    print "Accuracy % with sympy_mpmath_pi: {:.4g}".format(accuracy)

    return my_pi

# 2.28 s for 1000 correct digits
# just ... too long for 10000 correct digits
# Not so efficient method!
my_pi = bellard_method(1000, 1000)
print "This Bellar method is good also, with 2.28 s for 1000 correct digits."
print "But it tends to be slow for more digits..."


# %% Chudnovsky brothers' formula
# Source: https://en.wikipedia.org/wiki/Chudnovsky_algorithm
from math import factorial

def chudnovsky(max_step):
    print "Computing π with the Chudnovsky brothers' formula, upto {} steps.".format(max_step)
    my_pi = Decimal(0)
    for k in xrange(max_step):
        my_pi += (Decimal(-1)**k)*(Decimal(factorial(6*k))/((factorial(k)**3)*(factorial(3*k)))* (13591409+545140134*k)/(640320**(3*k)))
    my_pi = my_pi * Decimal(10005).sqrt()/4270934400
    my_pi = my_pi**(-1)
    return my_pi


def chudnovsky_method(precision, max_step):
    print "Computing π with the Chudnovsky brothers' formula with Decimal numbers, upto {} steps and with a desired precision of {} numbers.".format(max_step, precision)

    getcontext().prec = 3 + precision  # trick to improve precision
    my_pi = chudnovsky(max_step)
    print "π is approximately: {}.".format(my_pi)

    accuracy = 100*abs(Decimal(math.pi) - my_pi)/Decimal(math.pi)
    print "Accuracy % with math.pi: {:.4g}".format(accuracy)

    accuracy = 100*abs(sympy_mpmath_pi - my_pi)/sympy_mpmath_pi
    print "Accuracy % with sympy_mpmath_pi: {:.4g}".format(accuracy)

    return my_pi


# 17.4 s for 1000 correct digits
# just ... too long for 10000 correct digits
# Not so efficient method!
my_pi = chudnovsky_method(1000, 1000)
print "The Chudnovsky brothers' formula is slower here, with 17.4 s for 1000 correct digits."
print "And it tends to be really slow for more digits..."


# %% Machin's formula
# Source: http://en.literateprograms.org/Pi_with_Machin%27s_formula_%28Python%29

#mp.dps = 1000050  # number of digits, more than 1 million!
# mp.pi fail for such a big number of digits...
mp.dps = 100050  # number of digits
my_pi = mp.pi
# Saving it for further comparison of simpler methods
sympy_mpmath_pi = Decimal(str(my_pi))


# %%
def arccot(x, unity):
    mysum = xpower = unity / x
    n = 3
    sign = -1
    while True:
        xpower = xpower / (x*x)
        term = xpower / n
        if not term:
            break
        mysum += sign * term
        sign = -sign  # we alternate the sign
        n += 2
    return mysum


def machin(digits):
    unity = 10**(digits + 10)
    my_pi = 4 * (4*arccot(5, unity) - arccot(239, unity))
    return my_pi / Decimal(unity)


def machin_method(precision):
    print "Computing π with the Machin formula with a desired precision of {} numbers.".format(precision)

    getcontext().prec = 3 + precision  # trick to improve precision
    my_pi = machin(precision)
    print "π is approximately: {}.".format(my_pi)

    accuracy = 100*abs(Decimal(math.pi) - my_pi)/Decimal(math.pi)
    print "Accuracy % with math.pi: {:.4g}".format(accuracy)

    accuracy = 100*abs(sympy_mpmath_pi - my_pi)/sympy_mpmath_pi
    print "Accuracy % with sympy_mpmath_pi: {:.4g}".format(accuracy)

    return my_pi


# 10.2 ms for 1000 correct digits
# 841 ms for 10000 correct digits
my_pi = machin_method(10000)

# 22.2 s for 100000 correct digits
#%timeit my_pi = machin_method(100000)  # 1 lakh!
# Too long s for 1000000 (1 million) correct digits
#%timeit my_pi = machin_method(100000)  # 1 million!

print "The Machin method is best method implemented here, clearly, with only 841 ms for 1000 correct digits and only 22 s for 100000 digits."
print "But it is also getting too slow for more than 100000 digits... (more than 2 minutes for 1 million)."


# %% Unbounded Spigot Algorithm by Jeremy Gibbons
# Source: http://www.cs.ox.ac.uk/people/jeremy.gibbons/publications/spigot.pdf
# See this page (http://codepad.org/3yDnw0n9) for a 1-line C program that uses a simpler Spigot algorithm for computing the first 15000 digits

# Nice method with a generator (yielding the next digit each time)
# From http://stackoverflow.com/a/9005163
def next_pi_digit(max_step):
    q, r, t, k, m, x = 1, 0, 1, 1, 3, 3
    for j in xrange(max_step):
        if 4 * q + r - t < m * t:
            yield m
            # More details on Python generators can be found hre http://stackoverflow.com/a/231855
            q, r, t, k, m, x = 10*q, 10*(r-m*t), t, k, (10*(3*q+r))//t - 10*m, x
        else:
            q, r, t, k, m, x = q*k, (2*q+r)*x, t*x, k+1, (q*(7*k+2)+r*x)//(t*x), x+2


def generator_pi(max_step):
    digits = []
    # We use the previously defined generator to write this as a beautiful and elegant for loop
    for i in next_pi_digit(max_step):
        digits.append(str(i))

    digits = digits[:1] + ['.'] + digits[1:]  # long!
    big_string = "".join(digits)  # long!
    my_pi = Decimal(big_string)
#    print "here is a big string:\n %s" % big_string
    return my_pi


def generator_pi_method(precision, max_step):
    print "Computing π with the Unbounded Spigot Algorithm with a desired precision of {} numbers.".format(precision)

    getcontext().prec = 3 + precision  # trick to improve precision
    my_pi = generator_pi(max_step)
    print "π is approximately: {}.".format(my_pi)

    accuracy = 100*abs(Decimal(math.pi) - my_pi)/Decimal(math.pi)
    print "Accuracy % with math.pi: {:.4g}".format(accuracy)

    accuracy = 100*abs(sympy_mpmath_pi - my_pi)/sympy_mpmath_pi
    print "Accuracy % with sympy_mpmath_pi: {:.4g}".format(accuracy)

    return my_pi


# 700 ms for 1000 correct digits
# 9.19 s for 10000 correct digits
#my_pi = generator_pi_method(1000, 4200)
#%timeit my_pi = generator_pi_method(10000, 44000)
my_pi = generator_pi_method(1000, 4800)

print "The Unbounded Spigot Algorithm is quite efficient here, with 700 s for 1000 correct digits and 9.18 s for 10000 digits."
print "And it tends to be not so slow for more digits..."


# %% A random Monte-Carlo method (inefficient but interesting)
# Source: https://en.wikipedia.org/wiki/Pi#Geometry_and_trigonometry

import random

def random_point(radius):
    x = random.uniform(0, radius)
    y = random.uniform(0, radius)
    return (x, y)


def monte_carlo(max_step):
    radius = 1.0
    nb_of_good_points = 0
    for i in xrange(max_step):
        x, y = random_point(radius)
        if (x**2 + y**2) < radius**2:
            nb_of_good_points += 1
            # random point is inside the circle !
    return 4 * float(nb_of_good_points) / float(max_step)


def monte_carlo_method(max_step):
    print "Computing π with the a simple Monte-Carlo method, upto {} steps (no control over the precision).".format(max_step)

    my_pi = monte_carlo(max_step)
    print "π is approximately: {}.".format(my_pi)

    accuracy = 100*abs(Decimal(math.pi) - Decimal(my_pi))/Decimal(math.pi)
    print "Accuracy % with math.pi: {:.4g}".format(accuracy)

    accuracy = 100*abs(sympy_mpmath_pi - Decimal(my_pi))/sympy_mpmath_pi
    print "Accuracy % with sympy_mpmath_pi: {:.4g}".format(accuracy)

    print "Warning: Monte Carlo methods for approximating π are very slow compared to other methods, and are never used to approximate π when speed or accuracy are desired."

    return my_pi

# 1.69 s for 1 million random points, with a precision of 0.01% : just ridiculous in comparison to all the previous methods
my_pi = monte_carlo_method(100000)


# %% Borwein's cubic method
# Source: https://en.wikipedia.org/wiki/Borwein%27s_algorithm

def borwein(max_step):
    cst_1, cst_2, cst_3 = Decimal(1.0), Decimal(2.0), Decimal(3.0)
    a = cst_1 / cst_3
    s = (cst_3.sqrt() - cst_1)/cst_2
    for k in xrange(max_step):
        r = cst_3 / (cst_1 + cst_2 * (cst_1 - s**3)**(cst_1/cst_3))
        s = (r - cst_1) / cst_2
        a = r**2 * a - cst_3**k * (r**2 - cst_1)
    my_pi = cst_1/a
    return my_pi


def borwein_method(precision, max_step):
    print "Computing π with the Borwein's cubic algorithm with Decimal numbers, upto {} steps and with a desired precision of {} numbers.".format(max_step, precision)

    getcontext().prec = 10 + precision  # trick to improve precision
    my_pi = borwein(max_step)
    print "π is approximately: {}.".format(my_pi)

    accuracy = 100*abs(Decimal(math.pi) - my_pi)/Decimal(math.pi)
    print "Accuracy % with math.pi: {:.4g}".format(accuracy)

    accuracy = 100*abs(sympy_mpmath_pi - my_pi)/sympy_mpmath_pi
    print "Accuracy % with sympy_mpmath_pi: {:.4g}".format(accuracy)

    return my_pi

# 7.5 s for 1000 correct digits
#%timeit my_pi = borwein_method(1000, int(1000**(1./3.)))
# just ... too long for 10000 correct digits
%timeit my_pi = borwein_method(10000, int(10000**(1./3.)))
print "This Borwein method is good also, with 7.5 s for 1000 correct digits."
print "But it tends to be slow for more digits..."


# %% Conclusion
print "The simulations are done. Feel free to implement more method, and try to increase the efficiency of the methods implemented above."
print "\n\nHappy Pi Day ! (http://www.piday.org/)."
