# Python Demos
This repository hosts many examples of programs written in [Python 2.7](https://docs.python.org/2/), for the [CS101 Course](http://www.mahindraecolecentrale.edu.in/portal/course/view.php?id=27) at [Mahindra Ecole Centrale](http://www.mahindraecolecentrale.edu.in/) (spring semester 2015).

---

## This is **not** an introduction to Python
Before looking at the examples provided here, be sure to **be familiar with the bases of Python 2.7**.
If needed, work as much as you can on [this website introToPython.org](http://introtopython.org/):

 - [Variables, Strings, and Numbers](http://introtopython.org/var_string_num.html),
 - [Lists and Tuples](http://introtopython.org/lists_tuples.html),
 - [Introducing Functions](http://introtopython.org/introducing_functions.html),
 - [If Statements](http://introtopython.org/if_statements.html),
 - [While Loops and Input](http://introtopython.org/while_input.html),
 - [Dictionaries](http://introtopython.org/dictionaries.html),
 - [Classes](http://introtopython.org/classes.html),
 - [and some external resources](http://introtopython.org/resources.html).

----

## So what is this about? And why?
In February 2015, some students asked me about the possibility of frequently sharing small and interesting Python programs.

The reason seems to be that some of you want to be able to see what Python can *really* be used to do, and how does a real world Python program look like (a program designed to solve a concrete problem and not just an example in the lab).

### **Warning:** this is external to the CS101 course
All these examples are **NOT related to what will be covered in CS101 lectures and labs**, and are **NOT related to what will be asked in the lab exams and written exams**.
I am sharing all these demos only for *interested students* who would like to discover *a little more*.

These examples and demos are here *only to show you what Python can do* when used cleverly for some examples of scientific computing, problem solving, plotting etc (a list of covered domain is given below).

But reading one or two "real world" Python example from time to time might help you, mainly by triggering your interest.
No need for spending time on each programs, no need for reading the documentation of the modules I used, and no need of asking me endless questions about how I did on thing, why I did another one etc.

> But, please, [feel free to contact me if needed](http://perso.crans.org/besson/contact/en), or by email at `CS101 at crans dot org` (obviously, replace ` at ` with `@` and ` dot ` with `.`).

----

### And that might be true: reading examples can help you
[I (Prof. Lilian)](http://perso.crans.org/besson/) quickly created a git repository from my *"Python Scripts"* [Spyder](https://pythonhosted.org/spyder/) folder that I use on Windows, and shared all (almost all) the programs written from January 2015.

As far as now, these examples are *publicly* available at [bitbucket.org/lbesson/python-demos](https://bitbucket.org/lbesson/python-demos).
And you can **subscribe to its news feed** at [goo.gl/MesGzF](http://goo.gl/MesGzF).

[I](http://perso.crans.org/besson/) will really try to produce one or two new programs every week (Friday/Saturday usually).

> And you can even send me what you write, I will probably change it a little bit, and upload it here!

### Many examples from *various* domains (which are NOT in the syllabus for CS101)
It currently contains **about 45 examples**, covering:

 - some plotting examples: [a demo with 13 different examples (2D and 3D)](https://bitbucket.org/lbesson/python-demos/src/master/DemoPylab.py), and [one big demo based on one exercise of the tutorial sheet on 2D/3D integral (intersection of two paraboloid)](https://bitbucket.org/lbesson/python-demos/src/master/DemoTwoVolumes.py) with a the volume being "automatically" computed (with [NumPy](http://www.numpy.org)+[MatPlotLib](http://matplotlib.org/), as [the pie/bar charts for the CS101 first lab exam](https://bitbucket.org/lbesson/python-demos/src/master/grades_labExam_A1_group.py)). [This other program plots a visual representation of the Riemann rectangles](https://bitbucket.org/lbesson/python-demos/src/master/Plot_Riemann_rectangles.py), used in [a method for computing integrals](https://en.wikipedia.org/wiki/Riemann_sum#Methods). See [here for a tutorial on MatPlotLib](https://scipy-lectures.github.io/intro/matplotlib/matplotlib.html). **Only this part will be shortly studied with you for CS101**.
 - more examples of plotting: [this fun example](https://bitbucket.org/lbesson/python-demos/src/master/Fun_demo_MatPlotLib_XKCD_style.py)  was my starting point for the CS101 lectures on scientific plotting (XKCD style).
 - some simple algorithms and computation ([quick exponentation](https://bitbucket.org/lbesson/python-demos/src/master/powRec.py), [computations about the factorial and Fibonacci series](https://bitbucket.org/lbesson/python-demos/src/master/CS101_FirstLabExam_Solutions.py), [list sorting (lab #9)](https://bitbucket.org/lbesson/python-demos/src/master/CS101_Lab_Week_9__List_algorithms_and_sorting.py), and more recently [the sum and product of matrices (lab #10)](https://bitbucket.org/lbesson/python-demos/src/master/CS101_Lab_Week_10__Matrices_operations.py), **as studied in CS101 already**).
 - solutions for the CS101 2nd Mid-Term Exam [problem I](https://bitbucket.org/lbesson/python-demos/src/master/CS101_Second_Mid-Term_Exam__Problem_I.py), [problem II (quick exponentation for matrices)](https://bitbucket.org/lbesson/python-demos/src/master/CS101_Second_Mid-Term_Exam__Problem_II.py), [problem III (averages and weighted averages)](https://bitbucket.org/lbesson/python-demos/src/master/CS101_Second_Mid-Term_Exam__Problem_III.py).
 - [example of implementation](https://bitbucket.org/lbesson/python-demos/src/master/CS101_Lab_Week_9__Demo_of_the_Merge_Sort.py) of the [Merge-Sort algorithm](https://en.wikipedia.org/wiki/Merge_sort), as used in our lecture for CS101 and our 2nd Mid-Term exam.
 - bits of linear algebra ([demo about inner products](https://bitbucket.org/lbesson/python-demos/src/master/DemoInnerProducts.py), [or dimension of span generated by some vectors](https://bitbucket.org/lbesson/python-demos/src/master/MA102__Demo_Matrix_Rank_with_python.py), as you are studying it in MA102 (with [NumPy/SciPy](http://docs.scipy.org/doc/scipy/reference/linalg.html)). [We used this automatic generator](https://bitbucket.org/lbesson/python-demos/src/master/MA102__demo_generate_random_rank_with_python.py) of families of vectors of a certain rank (for a quiz in MA102).
 - I also wrote [the Gram-Schmidt process (on Rn)](https://bitbucket.org/lbesson/python-demos/src/master/Gram_Schmidt_simple.py), and [the Gauss elimination algorithm](https://bitbucket.org/lbesson/python-demos/src/master/Gauss_Elimination.py).
 - from MA102, some examples of [formal determinants (MA102 Sheet 8)](https://bitbucket.org/lbesson/python-demos/src/master/Computing_Abstract_Determinants_with_SymPy.py) or [eigen values and eigen vectors (MA102 Sheet 9)](https://bitbucket.org/lbesson/python-demos/src/master/Computing_Eigen_Values_and_Vectors_with_SymPy.py) (formally computed with SymPy).
 - some number related problems (like this one [about prime numbers](https://bitbucket.org/lbesson/python-demos/src/master/PrimeNumber.py) or [about large prime factor](https://bitbucket.org/lbesson/python-demos/src/master/largest_prime_factor.py), as studied in one CS101 lab assignment). See also [this solver for polynomials of order 1, 2 and 3](https://bitbucket.org/lbesson/python-demos/src/master/Cubic_Equation.py).
 - some symbolic computations: [a 2D line integral (as covered in MA102 part1)](https://bitbucket.org/lbesson/python-demos/src/master/DemoLineIntegrals.py) and [an 2D integral, demonstrating Fubini's theorem (to change the order of integration)](https://bitbucket.org/lbesson/python-demos/src/master/DemoSympy.py) (with [SymPy](http://www.sympy.org/en/index.html)).
 - [numerical solving](https://bitbucket.org/lbesson/python-demos/src/master/PDE_02_2015/pde_02_2015.py) of [a non-linear 1D PDE](http://perso.crans.org/besson/publis/PDE_09_2014) (the linked page is in French, sorry; but this will studied in your PDE course in 3rd year anyway).
 - some [black-box optimization using scipy.optimize](https://bitbucket.org/lbesson/python-demos/src/master/ScipyOptimize.py) (based on [this excellent tutorial](http://scipy-lectures.github.io/advanced/mathematical_optimization/)). You will do more of this *next year in the MA204 course*.
 - one [machine learning example](https://bitbucket.org/lbesson/python-demos/src/master/DemoSVN.py) (with [Sklearn](http://scikit-learn.org/), for hand-written digits recognition, with a SVN).
 - [implementation of one numerical method for computing an integral](https://bitbucket.org/lbesson/python-demos/src/master/Romberg_method_for_computing_1D_integrals.py), the [Romberg's method](https://en.wikipedia.org/wiki/Romberg%27s_method). [Another implementation with a recursive function](https://bitbucket.org/lbesson/python-demos/src/master/Recursive_Romberg_method_for_computing_1D_integrals.py).
 - some simple image processing: [detection of interesting parts (blobs)](https://bitbucket.org/lbesson/python-demos/src/master/BlobDetection.py), [computation of the edges of some images](https://bitbucket.org/lbesson/python-demos/src/master/DemoEdges.py), [segmentation of an image](https://bitbucket.org/lbesson/python-demos/src/master/DemoSegmentation.py) and [one example about entropy](https://bitbucket.org/lbesson/python-demos/src/master/DemoSkimage.py) (with [Skimage](http://scikit-image.org/)). The other module [scipy.ndimage provides also some nice features](https://bitbucket.org/lbesson/python-demos/src/master/Measurements_on_images_with_scipy_ndimage.py).
 - a [partial solution for](https://bitbucket.org/lbesson/python-demos/src/master/Computing_Pi.py) the [Pi Day Hackathon](http://perso.crans.org/besson/publis/hackathon/14_03_2015.pdf) I organized at [MEC](http://www.mahindraecolecentrale.edu.in/) for [Pi Day 2015](http://www.piday.org/). [I also implemented a simple Monte-Carlo](https://bitbucket.org/lbesson/python-demos/src/master/Monte-Carlo_method_for_computing_pi.py) method for approximating pi.
 - there is also [one example of the mayavi 3D plotting module](https://bitbucket.org/lbesson/python-demos/src/master/Demo_Mayavi_3D_toolkit.py).
 - and [one example of use of tuples, lists, sets, and dictionaries](https://bitbucket.org/lbesson/python-demos/src/master/CS101_Lab_Week_12__Lists_Tuples_Sets_Dicts.py) (CS101 Lab #12).
 - a small [toy program, playing with strings and caracters, sent by a student](https://bitbucket.org/lbesson/python-demos/src/master/Demo_strings_caracters.py).
 - I also tried the [mpld3 module](http://mpld3.github.io/quickstart.html) [for this example](https://bitbucket.org/lbesson/python-demos/src/master/mpld3_demo_MatPlotLib_in_the_browser.py).
 - a [small demo of the super() function for easy class inheritance](https://bitbucket.org/lbesson/python-demos/src/master/Demo_Class_inheritance_with_the_super_function.py) (thanks to one student for sharing her program).
 - two demonstrations of how to manually read a [gray-scale BMP image](https://bitbucket.org/lbesson/python-demos/src/master/Demo_reading_displaying_a_Bitmap_image.py), and [a colored BMP image](https://bitbucket.org/lbesson/python-demos/src/master/Demo_reading_displaying_a_Bitmap_color_image.py).
 - two examples of [Monte-Carlo method for computing integrals](https://en.wikipedia.org/wiki/Monte_Carlo_integration): [for 1D integrals](https://bitbucket.org/lbesson/python-demos/src/master/Monte-Carlo_method_for_computing_an_1D_integral.py) or [2D integrals (on a simple domain)](https://bitbucket.org/lbesson/python-demos/src/master/Monte-Carlo_method_for_computing_a_2D_integral.py).
 - [3 different examples](https://bitbucket.org/lbesson/python-demos/src/master/Examples_of_Guassian_quadratures_for_computing_1D_integrals.py) of [Gaussian quadrature](http://mathworld.wolfram.com/GaussianQuadrature.html) for computing 1D integrals.
 - [two examples of animated plots](https://bitbucket.org/lbesson/python-demos/src/master/Demo_of_a_MatPlotLib_animation.py) with [the MatPlotLib API for animation](http://matplotlib.org/api/animation_api.html) (a moving sine wave, and a shrinking star in 2D), based on [this tutorial](https://jakevdp.github.io/blog/2012/08/18/matplotlib-animation-tutorial/).


> The CS101 course is over, but maybe I will still write more examples, from time to time.

### Contact me if needed?
 - If you want a specific example, *please* contact me through Moodle, by email, [here on this form online](http://perso.crans.org/besson/contact/en).
 - If you found any issue in one of the programs shared here, or if you have any suggestion, feel free to [open a bug ticket](https://bitbucket.org/lbesson/python-demos/issues/new/) (it's free, easy, and open to anyone *without* being logged in).

----

## Other resources for the CS101 course
The main reference for you is [the CS101 page on the MEC Moodle portal](http://www.mahindraecolecentrale.edu.in/portal/course/view.php?id=27) (access restricted).

I also wrote a complete solution for two of the 6 programming project we gave at MEC:

 - [MEC CS101 : Integrals](https://mec-cs101-integrals.readthedocs.org/), the task was to implement various methods for computing definite integrals numericall (in 1D but not only),
 - [MEC CS101 : Matrices](https://mec-cs101-matrices.readthedocs.org/), a project about linear algebra operations and matrices.

---

### Project Euler
[This file](https://bitbucket.org/lbesson/python-demos/src/nope_do_not_spoil_yourself/ProjectEuler.py) contains my (current) solutions for the [Project Euler](https://ProjectEuler.net) challenge.

### /!\ **Warning:** do not spoil yourself the pleasure of solving these problems by yourself!!

[I (Prof. Lilian)](http://perso.crans.org/besson/) started in February 2015, and [usually solve one problem every week](https://ProjectEuler.net/profile/Naereen.png) (when I have the time!):
![Badge giving the number of solved problems](https://ProjectEuler.net/profile/Naereen.png "Badge giving the number of solved problems")

---

### Author | License ?
Except if something else is mentioned, all these files have been written by [Lilian Besson](http://perso.crans.org/besson/) and are copyrighted (C) 2015.
All these files are here publicly released, under the conditions of [GNU Public License v3](http://perso.crans.org/besson/LICENSE.html).

> [I might switch to the MIT License](http://lbesson.mit-license.org/), but this does not really change anything for you.
