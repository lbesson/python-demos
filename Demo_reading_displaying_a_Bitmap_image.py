#! /usr/bin/env python
# -*- coding: utf-8 -*-
"""
Demo of reading and displaying a BMP image (bitmap).

Reference for the file format is: https://en.wikipedia.org/wiki/BMP_file_format.

@date: Mon Apr 13 11:40:55 2015.
@author: Lilian Besson for CS101 course at Mahindra Ecole Centrale 2015.
@licence: MIT Licence (http://lbesson.mit-license.org).
"""

import struct

# %% Read and plot the image with MatPlotLib
if __name__ == '__main__':
    from matplotlib.pyplot import imshow, imread
    import matplotlib.pyplot as plt

    plt.figure()
    plt.title("Gray-scale Lena (read with MatPlotLib: imread('lena.bmp'))")
    myimage = imread("lena.bmp")  # , as_grey=True)
    imshow(myimage, cmap='gray')

    plt.savefig("lena_matplotlib_pyplot_imread.png")


# %% Do it manually
#path="lena.bmp"
def bmp_to_matrix(path="lena.bmp"):

    # Read the "lena.bmp" image in binary mode
    lena = open(path, 'rb')
    content = list(lena)


    # %% Reading the header
    header = content[0][:-1]
#    print "The image header has size", len(header), "bytes."
    # Check that it is a good BMP file
    if str(header[0:2]) not in ['BM', 'BA', 'CI', 'CP', 'IC', 'PT']:
        raise ValueError("bmp_to_matrix(): file ('{}') seems to not be a good BMP file.".format(path))

    # Reference is  https://en.wikipedia.org/wiki/BMP_file_format#Bitmap_file_header

    # Checking that the file size is correct
    size_bytes, = struct.unpack('<i', header[2:6])
    if size_bytes != sum([len(i) for i in content]):
        print "ERROR: the file seems to be corrupted (wrong size in header[2:6]."
        raise ValueError("bmp_to_matrix(): file ('{}') seems to be corrupted (wrong size in header[2:6].".format(path))
    else:
        print "The BMP image is stored with", size_bytes, "bytes, which is about", size_bytes / 1024, "kB."

    # Finding the offset
    offset, = struct.unpack('<i', header[10:14])
    print "The bitmap image data (pixel array) can be found from the bytes at the offset +", offset, "bytes."

    # We now read the Windows BITMAPINFOHEADER (cf. Wikipedia)
    bitmap_width, = struct.unpack('<i', header[18:22])  # 4-byte integer
    bitmap_height, = struct.unpack('<i', header[22:26])  # 4-byte integer
    bits_per_pixel, = struct.unpack('<h', header[28:30])  # short integer

    print "According to its header, the image has a width of", bitmap_width, "pixels, and a height of", bitmap_height, "pixels."
    print "And each pixel uses", bits_per_pixel, "bits, ie", bits_per_pixel/8, "bytes : so it is an integer value from 0 to 2 **", bits_per_pixel, "- 1, ie 0 to", ((2**bits_per_pixel)-1), "for", 2**bits_per_pixel, "different colours/scales of gray."

    # Checking that the image is indeed a pixel array of this size
    assert bitmap_width * bitmap_height == size_bytes - offset


    # %% Reading the content (after the offset)
    bigstring = "".join(content)[offset:]
    n = len(bigstring)
    #print bigstring
    print "We have", n, "bytes in this pixel array."

    # Now we have one big string, representing the pixel array


    # %% Trying to convert from bytes to integer

    # In our "lena.png" example:
    # The 8-bit per pixel (8bpp) format supports 256 distinct colors and stores 1 pixel per 1 byte.

    pixels = list(struct.unpack('<' + 'B'*n, bigstring))
    # Grey-scale (8-bits integer, ie. 1-byte integer)
    # More details https://docs.python.org/2/library/struct.html?highlight=struct%20module#format-characters

    assert all( 0 <= p < 2**bits_per_pixel for p in pixels)
    #print pixels

    # We need to be cautious
    # Normally pixels are stored "upside-down" with respect to normal image raster scan order, starting in the lower left corner, going from left to right, and then row by row from the bottom to the top of the image.

    mat_pixels = [ [ pixels[j + i*bitmap_height] for j in xrange(bitmap_height) ] for i in xrange(bitmap_width-1, -1, -1) ]

#    print mat_pixels

    # We just check that it has been read correctly
    plt.figure()
    plt.title("Gray-scale Lena (manually read from the file 'lena.bmp')")
    plt.imshow(mat_pixels, cmap='gray')

    plt.savefig("lena_manually_read.png")

    return mat_pixels


# %% Test it
if __name__ == '__main__':
    lena_matrix = bmp_to_matrix("lena.bmp")
