#! /usr/bin/env python
# -*- coding: utf-8 -*-
"""
Simple Monte-Carlo method for computing pi/4.

@date: Fri Mar 13 14:56:29 2015.
@author: Lilian Besson for CS101 course at Mahindra Ecole Centrale 2015.
@licence: MIT Licence (http://lbesson.mit-license.org).
"""

from random import uniform
nbPoints = 1000000
nbInside = 0

for i in range(nbPoints):
    x = uniform(0, 1)
    y = uniform(0, 1)
    if (x**2 + y**2) <= 1:
        # This point (x, y) is inside the circle C(0, 1)
        nbInside += 1

pi = 4 * float(nbInside) / float(nbPoints)
print "The simple Monte-Carlo method with", nbPoints, "random points gave pi ≈", pi
