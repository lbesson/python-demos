#! /usr/bin/env python
# -*- coding: utf-8 -*-
"""
Main program, where we import the matrix package.
For the CS101 Lab sheet #13.

@date: Tue Mar 31 14:24:15 2015.
@author: Lilian Besson for CS101 course at Mahindra Ecole Centrale 2015.
@licence: MIT Licence (http://lbesson.mit-license.org).
"""

import matrix

A = [ [1, 3], [3, 5] ]
B = [ [2, 4], [7, -4] ]

print "A + B is", matrix.matrix_add(A, B)
print "A * B is", matrix.matrix_mult(A, B)

