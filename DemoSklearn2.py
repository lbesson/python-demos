#!/usr/bin/env python
# -*- coding: utf-8 -*-
# %%
from sklearn import datasets
iris = datasets.load_iris()

print(iris.data.shape)
print(iris.target.shape)

import numpy as np
np.unique(iris.target)

digits = datasets.load_digits()
print(digits.images.shape)

import pylab as pl
#pl.imshow(digits.images[0], cmap=pl.cm.gray_r, interpolation='none')

data = digits.images.reshape((digits.images.shape[0], -1))

from sklearn import svm
clf = svm.LinearSVC()
clf.fit(iris.data, iris.target) # learn from the data

print(clf.predict([[ 5.0,  3.6,  1.3,  0.25]]))
print(clf.coef_)


# Create and fit a nearest-neighbor classifier
from sklearn import neighbors
knn = neighbors.KNeighborsClassifier()
knn.fit(iris.data, iris.target)
print(knn.predict([[0.1, 0.2, 0.3, 0.4]]))

#perm = np.random.permutation(iris.target.size)
#iris.data = iris.data[perm]
#iris.target = iris.target[perm]
print(knn.fit(iris.data[:100], iris.target[:100]))

print(knn.score(iris.data[100:], iris.target[100:]))

from sklearn import svm
svc = svm.SVC(kernel='linear')
svc.fit(iris.data, iris.target)