#! /usr/bin/env python
# -*- coding: utf-8 -*-
"""
Demo of PyLab for doing easy plotting for CS101 grades.
Way easier and nicer than Excel...

You can work on it by yourself to pick up bases of plotting with Python and PyLab.


@date: Wed Feb 25 17:38:00 2015.
@author: Lilian Besson for CS101 course at Mahindra Ecole Centrale 2015.
@licence: GNU Public License version 3.
"""

# We import the PyLab module, which turns out Python as a MatLab clone
from pylab import *

# List of grades, included row from the Excel sheet
grades_from_excel = [ 36, 73.5, 51, 33, 66.5, 57.5, 46, 49, 71, 68, 27, 74, 56, 56.5, 90, 51, 46, 66.5, 62, 93, 86.5, 89.5, 39, 68, 89, 45, 75, 48.5, 33, 72, 61, 67.5, 93.5, 66, 64, 90, 58, 68, 64, 48, 49.5, 50, 57, 47, 51, 72, 40, 34, 58, 70, 49, 56, 94, 50, 67.5, 71.5, 58, 56.5, 33, 47.5, 66.5, 67, 86, 24, 68, 59.5, 47, 43, 41, 57, 77, 16, 77.5, 47, 61, 25, 62, 82, 48, 58, 64, 59.5, 62, 24, 60, 26, 57.5, 68.5, 61, 28, 48.5, 85.5, 35, 25, 73, 44.5, 79, 83.5, 36, 49, 63, 57, 51, 30, 33.5, 61, 72.5, 76, 73, 36, 75.5, 51, 26, 52, 24, 37.5, 80, 30, 63, 48, 88, 36, 45, 63.5, 69, 26, 52, 61, 50.5, 59, 46, 59, 55, 60, 63, 53, 20, 70, 74.5, 42, 80, 41.5, 54.5, 73, 40.5, 82, 36, 69, 48.5, 51, 63, 67, 60.5, 78, 58.5, 50.5, 62, 22, 53.5, 65, 78, 60.5, 32, 88.5, 74, 33, 53, 53, 83, 59, 74, 53, 55, 76, 71, 64, 58, 56.5, 40, 29, 33.5, 58.5, 54, 63, 52.5, 45, 88, 61, 90.5, 65, 33, 49, 70.5, 39.5, 53, 17, 75.5, 55, 58, 59, 36, 49.5, 47, 53, 77, 68, 83, 35.5, 51, 44, 35, 52, 58, 60.5, 82, 57.5, 45, 45, 64.5, 35, 54.5, 75, 69, 54.5, 59, 72.5, 51.5, 76, 68, 61, 29 ]

# Number-1 of parts of the pie chart. 10 is good
nb = 10

# We create a dictionnary, to count how many students were in each range [0%, 10%), [10%, 20%) etc
count = {nb*i: 0 for i in range(0, nb+1)}
# This uses dict comprehension, cf. https://www.python.org/dev/peps/pep-0274/ for more details


# We convert the grades into integers 0, 1, .., 10
grades = [nb*(floor(g/nb)) for g in grades_from_excel]
# This uses list comprehension, cf. https://www.python.org/dev/peps/pep-0202/
# Or https://en.wikipedia.org/wiki/List_comprehension#Python for one simple example


# And we count
for g in grades:
    count[g] += 1  # One more student had his grade in THAT range [g, g+1)

# We display the result of this step
# The order of this for loop is random, it will NOT print [0, 10), [10, 20) etc.
for g, c in count.items():
    print c, "students got a mark between", g, "% and", g+1, "%."

    # We remove ranges with no students, # As we do not want to print empty parts on the pie chart
    if c == 0:
        count.pop(g)


# I want the parts in the pie chart to be sorted, so let us sort the keys
keys = count.keys()
keys.sort()  # This method will sort IN-PLACE the list keys

# Create the labels, again with list comprehension
# And string formatting (see https://docs.python.org/2/library/stdtypes.html#str.format)
labels = ["{} marks in [{}%, {}%]".format(count[k], k, k+10) for k in keys]


# Create the data that will be plotted
x = [count[k] for k in keys]  # one more example of list comprehension !


# Create the range of colors (grey) used for the pie chart
from matplotlib.colors import ColorConverter
cc = ColorConverter()  # technical boring step
# More details on http://matplotlib.org/api/colors_api.html#matplotlib.colors.ColorConverter.to_rgb

colors = [cc.to_rgb(str(1.0-k/100.0)) for k in keys]  # this will produce a scale of gray


## Now we (finally) make the pie chart
#fig = figure(1)
#
## With a title
#title("Pie chart representation of the performance for CS101 First Mid Term Exam")
#
#pie(x,  # Using the list x as data
#    labels=labels,  # The labels we created
#    autopct="%i%%",  # A format string to be used for writing a label inside each wedge
#    explode=[0.1]*len(x),  # Explode the pie chart
#    colors=colors)  # And we use that range of gray colors
#
## Finally, we save the plot to a PNG image
#savefig("Pie_chart_representation_of_the_performance_for_CS101_First_Mid_Term_Exam.png")
## And a PDF document
#savefig("Pie_chart_representation_of_the_performance_for_CS101_First_Mid_Term_Exam.pdf")
#
#print "We are done plotting, and two files has been produced in the current directory (one .png and one .pdf)."
#print "An exercise for you can be to produce a colored drawing (change the way the colors list is created)."


fig = figure(2)
title("Histogram of the performance for CS101 First Mid Term Exam")
xlabel(r"Grades between 0% and 100%.")
ylabel("Number of students with that grades.")
ylim(0, max(x)+1)

bar(keys,  # this is the data for x axis
    x, # this is the data for y axis
    color=colors, width=nb*0.9)
for xpixel, ypixel in zip(keys, x):
    text(xpixel+nb/2, ypixel+0.05, '%i students' % ypixel, ha='center', va='bottom')

# Finally, we save the second plot to a PNG image
savefig("Histogram_of_the_performance_for_CS101__First_Mid_Term_Exam.png")
# And a PDF document
savefig("Histogram_of_the_performance_for_CS101__First_Mid_Term_Exam.pdf")

print "Another exercise could be to generate a pie plot also."
