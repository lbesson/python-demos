#! /usr/bin/env python
# -*- coding: utf-8 -*-
"""
CS101 Lab #12, about lists/tuples, sets and dictionaries.
(Partial solution)

@date: Wed Apr 01 08:38:38 2015.
@author: Lilian Besson for CS101 course at Mahindra Ecole Centrale 2015.
@licence: MIT Licence (http://lbesson.mit-license.org).
"""


# %% Lists and tuples
colours = [ "red", "yellow" ]
c = ( "white", "red" )

# Accessing
print "Reading a list:", colours[0]
print "Reading a tuple:", c[0]


# Modifying
colours[1] = "pink"  # OK
print colours

try:
    c[0] = "Naranja"  # Fails!
    c.append("Vert")  # Fails!
except:
    print "A tuple is not mutable ... !"


# %% List of tuples (i, i**3)

l = []
for i in xrange(1, 11):
    l.append( (i, i**3) )
print l

# List comprehension is also good !
l = [ (i, i**3) for i in xrange(1,11) ]

print "This list of tuples (i,i**3) is", l

# Do a for loop over this list of tuples
for smalltuple in l:
    print smalltuple


# %% Sets

print "\nThree kind of employees in our compagny:"
engs = { "Mahesh", "Mohan", "Nikhita", "Nishita" }
progs = { "Likitha", "K.", "Moresh" }
mags = { "Dev.", "Hari", "Mohan" }

print "There is", len(engs), "engineers."
print "There is", len(progs), "programmers."
print "There is", len(mags), "managers."

if "Moresh" in progs:
    print "My friend Moresh is a programmer."
else:
    print "My friend Moresh is not a programmer... but I still like him :)"

# Union
employees = engs | progs | mags
employees = engs.union(progs, mags)

print "Set of employees:", employees

# Subset or superset
#if employees >= engs:
if employees.issuperset(engs):
    print "All engineers are employees :)"
else:
    print "WHAT? One engineers is jobless ?"


# %% Set comprehension vs list comprehension

print "\nFirst example is a list, with 4*4 elements."
mylist = [ a+b for a in xrange(1, 5) for b in xrange(6, 10) ]
print mylist
print "This list has", len(mylist), "elements"

print "Second example is that list seen as set, with less than 4*4 elements !"
myset = { a+b for a in xrange(1, 5) for b in xrange(6, 10) }
print myset
print "This set has", len(myset), "elements"


# %% Dictionary
print "\nWorking with this dictionary:"
europe = { "France": "Fr",
           "Germany": "DE",
           "Italy": "IT"
         }
print europe

# Reading one key:value pair
print europe["France"]  # 'Fr'

# Updating the dictionnary
print "Before", europe
europe["France"] = "FR"
print "After", europe

# Adding one key:value to the dict
europe["Belgium"] = "BE"
print "One more:", europe

# Deleting a key from the dict
del europe["Germany"]
print "Europe without Germany is now", europe


# %% Looping over a dictionary
# 1st way : loop over the keys

for country in europe:
    print "The country", country, "has code", europe[country]


# %% Application of dictionnary

print "\nCounting occurences of letters of that string"
myword = "Superman is a good superhero"
print myword

letters = {}
for carac in myword:
    letters[carac] = letters.get(carac, 0) + 1

print letters
