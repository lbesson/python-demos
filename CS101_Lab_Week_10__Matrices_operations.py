#! /usr/bin/env python
# -*- coding: utf-8 -*-
"""
CS101 Lab solution for week 10 : matrices operations (bases).

@date: Sat Mar 7 18:07:23 2015.
@author: Lilian Besson for CS101 course at Mahindra Ecole Centrale 2015.
@licence: GNU Public License version 3.
"""

A = [[0, 1], [2, 3]]
B = [[4, 5], [3, 5]]


# %% Problem 9.1 : sum of two matrices

def add_matrix(A, B):
    """ Add two square matrices A and B (of the same size).
    There is O(n^2) elementary additions, and O(n^2) memory accesses (then all take a worst case time of O(n) because all the lists are of size n).
    The function uses an extra matrix C of size O(n^2).
    """
    n = len(A)
    # We create an empty matrix of size (n, n), filled with zero
    C = [ [0]*n ]*n
    # We assume that A and B are square matrices
    for i in xrange(n):
        for j in xrange(n):
            C[i][j] = A[i][j] + B[i][j]
    # Finally this matrix C is returned
    return C


print add_matrix(A, B)

# add_matrix = lambda A, B: [ [ A[i][j] + B[i][j] for j in xrange(len(A)) ] for i in xrange(len(A)) ]


# %% Problem 9.2 : sum of two matrices

def product_matrix(A, B):
    """ Multiply two square matrices A and B (of the same size).
    There is O(n^2) additions and O(n^3) multiplications, and O(n^3) memory accesses (then all take a worst case time of O(n) because all the lists are of size n)
    The function uses an extra matrix C of size O(n^2).
    """
    n = len(A)
    # We create an empty matrix of size (n, n), filled with zero
    C = [ [0]*n ]*n
    # We assume that A and B are square matrices
    for i in xrange(n):
        for j in xrange(n):
            C[i][j] = sum([ A[i][k] * B[k][j] for k in xrange(n) ])
    # We use the builtin sum function, with a list comprehension to avoid a 3rd loop
    # Finally this matrix C
    return C


print product_matrix(A, B)

# product_matrix = lambda A, B: [ [ sum([ A[i][k] * B[k][j] for k in xrange(len(A)) ]) for j in xrange(len(A)) ] for i in xrange(len(A)) ]


# %% Problem 9.4 : random integer matrices

import random

def random_matrix(n):
    """ Random matrix of size (n, n) filled with integers taken in [-10, 10].
    """
    # We create an empty matrix of size (n, n), filled with zero
    C = [ [0]*n ]*n
    for i in xrange(n):
        for j in xrange(n):
            C[i][j] = random.randint(-10, 10)
    return C


# from random import randint
# random_matrix = lambda n: [ [ random.randint(-10, 10) for j in xrange(n) ] for i in xrange(n) ]


# %% Problem 9.5 : comparing times executions

print "For the addition (in O(n**2)): first for n=100 then n=1000."
#%timeit add_matrix(random_matrix(100), random_matrix(100))
#%timeit add_matrix(random_matrix(1000), random_matrix(1000))


print "For the multiplication (in O(n**3)): first for n=100 then n=200."
#%timeit product_matrix(random_matrix(100), random_matrix(100))
#%timeit product_matrix(random_matrix(200), random_matrix(200))
